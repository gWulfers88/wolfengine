#include "Window.h"
#include "Errors.h"

namespace WolfEngineGL
{

	Window::Window()
		: m_pWindow(nullptr),
		m_ScrnWidth(0),
		m_ScrnHeight(0)
	{
	}

	Window::~Window()
	{
	}

	int Window::CreateWindow(std::string windowName, int screenWidth, int screenHeight, unsigned int currentFlags)
	{
		Uint32 flags = SDL_WINDOW_OPENGL;
		m_ScrnWidth = screenWidth;
		m_ScrnHeight = screenHeight;

		if (currentFlags & INVISIBLE)
		{
			flags |= SDL_WINDOW_HIDDEN;
		}

		if (currentFlags & FULLSCREEN)
		{
			flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
		}

		if (currentFlags & BORDERLESS)
		{
			flags |= SDL_WINDOW_BORDERLESS;
		}

		std::string newName = "";

		if (windowName != SCRN_NAME)
		{
			newName = SCRN_NAME + windowName;
		}
		else
		{
			newName = windowName;
		}

		m_pWindow = SDL_CreateWindow(newName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			screenWidth, screenHeight, flags);

		if (m_pWindow == nullptr)
		{
			fatalError("SDL Window could not be created!");
		}

		SDL_GLContext glContext = SDL_GL_CreateContext(m_pWindow);

		if (glContext == nullptr)
		{
			fatalError("SDL_GL Context could not be created!");
		}

		GLenum error = glewInit();

		if (error != GLEW_OK)
		{
			fatalError("Could initialize Glew!");
		}

		//Checks OpenGL Version
		printf("*** OpenGL Version %s ***\n", glGetString(GL_VERSION));

		glClearColor(0, 0, 0.5f, 1.0f);

		//Sets VSYNC on
		SDL_GL_SetSwapInterval(1);

		//Enable alpha blending
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		return 0;
	}

	void Window::SwapBuffers()
	{
		SDL_GL_SwapWindow(m_pWindow);
	}

	int Window::GetScreenWidth()
	{
		return m_ScrnWidth;
	}

	int Window::GetScreenHeight()
	{
		return m_ScrnHeight;
	}

	SDL_Window* Window::GetWindow()
	{
		return m_pWindow;
	}
}