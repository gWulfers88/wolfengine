#include "Timing.h"

#include <SDL/SDL.h>

namespace WolfEngineGL
{
	FPSLimiter::FPSLimiter()
	{

	}

	FPSLimiter::~FPSLimiter()
	{

	}

	void FPSLimiter::Init(float newTargetFPS)
	{
		SetTargetFPS(newTargetFPS);
	}

	void FPSLimiter::SetTargetFPS(float newTargetFPS)
	{
		targetFPS = newTargetFPS;
	}

	void FPSLimiter::Start()
	{
		startTicks = SDL_GetTicks();
	}

	float FPSLimiter::Stop()
	{
		CalculateFPS();

		float frameTicks = (float)SDL_GetTicks() - startTicks;

		//Limit FPS
		if (1000.0f / targetFPS > frameTicks)
		{
			SDL_Delay((Uint32)((1000.0f / targetFPS) - frameTicks));
		}

		return fps;
	}

	void FPSLimiter::CalculateFPS()
	{
		static const int NUM_SAMPLES = 10;
		static float frameTimes[NUM_SAMPLES];
		static int currentFrame = 0;

		static float prevTicks = (float)SDL_GetTicks();

		float curTicks;

		curTicks = (float)SDL_GetTicks();

		frameTime = curTicks - prevTicks;

		frameTimes[currentFrame % NUM_SAMPLES] = frameTime;

		prevTicks = curTicks;

		int count;

		currentFrame++;

		if (currentFrame < NUM_SAMPLES)
		{
			count = currentFrame;
		}
		else
		{
			count = NUM_SAMPLES;
		}

		float framTimeAvrg = 0;

		for (int i = 0; i < count; i++)
		{
			framTimeAvrg += frameTimes[i];
		}

		framTimeAvrg /= count;

		if (framTimeAvrg > 0)
		{
			fps = 1000.0f / framTimeAvrg;
		}
		else
		{
			fps = 60.0f;
		}

	}
}