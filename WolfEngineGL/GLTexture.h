#pragma once

#include <GL/glew.h>

namespace WolfEngineGL
{
	struct GLTexture
	{
		GLuint ID;
		int Width;
		int Height;
	};
}