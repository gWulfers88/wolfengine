#include "ParticleSystem2D.h"
#include "ParticleBatch2D.h"
#include "SpriteBatch.h"

namespace WolfEngineGL
{

	ParticleSystem2D::ParticleSystem2D()
	{
	}


	ParticleSystem2D::~ParticleSystem2D()
	{
		for ( auto& b : m_batches)
		{
			delete b;
		}
	}

	void ParticleSystem2D::AddParticleBatch(ParticleBatch2D* particleBatch)
	{
		m_batches.push_back(particleBatch);
	}

	void ParticleSystem2D::Update(float deltaTime)
	{
		for (auto& batch : m_batches)
		{
			batch->Update(deltaTime);
		}
	}
	
	void ParticleSystem2D::Draw(SpriteBatch* spriteBatch)
	{

		for (auto& batch : m_batches)
		{
			spriteBatch->Begin();

			batch->Draw(spriteBatch);

			spriteBatch->End();

			spriteBatch->Render();
		}
	}

}