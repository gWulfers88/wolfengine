#pragma once

#include <vector>

namespace WolfEngineGL
{
	class IMainGame;
	class IGameScreen;

	class ScreenList
	{
	public:
		ScreenList(IMainGame* game);
		~ScreenList();

		IGameScreen* MoveNext();
		IGameScreen* MovePrev();
		IGameScreen* GetCurrent();

		void SetScreen(int nextScreen);
		void AddScreen(IGameScreen* newScreen);

		void Destroy();

	protected:
		IMainGame* m_game = nullptr;
		std::vector<IGameScreen*> m_screens;
		int m_currScrnIndex = -1;
	};

}