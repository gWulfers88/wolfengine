#include "ResourceManager.h"

namespace WolfEngineGL
{

	TextureCache ResourceManager::textureCache;

	GLTexture ResourceManager::GetTexture(std::string texturePath)
	{
		return textureCache.GetTexture(texturePath);
	}

}