#include "TextureCache.h"
#include "ImageLoader.h"
#include <iostream>

namespace WolfEngineGL
{

	TextureCache::TextureCache()
	{
	}


	TextureCache::~TextureCache()
	{
	}

	GLTexture TextureCache::GetTexture(std::string texturePath)
	{
		//look up tex to see if it is in map
		auto mit = textureMap.find(texturePath);

		if (mit == textureMap.end())
		{
			GLTexture newTex = ImageLoader::LoadPNG(texturePath);

			std::pair<std::string, GLTexture> newPair(texturePath, newTex);

			textureMap.insert(newPair);

			std::cout << "Loaded texture." << std::endl;

			return newTex;
		}

		std::cout << "Used cached texture." << std::endl;
		return mit->second;
	}

}