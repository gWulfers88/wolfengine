#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace WolfEngineGL
{
	class Camera2D
	{
	public:
		Camera2D();
		~Camera2D();

		void Init(int screenWidth, int screenHeight);
		void Update();

		glm::vec2 GetWorldCoordsFromScreen( glm::vec2 screenCoords );

		void SetPosition(const glm::vec2& newPosition);
		glm::vec2 GetPosition();

		void OffsetPosition(const glm::vec2& offset) { position += offset; needsMatrixUpdate = true; }
		void OffsetScale(float offset) { scale += offset; if (scale < 0.001f) scale = 0.001f; needsMatrixUpdate = true; }

		void SetScale(float newScale);
		float GetScale();

		glm::mat4 GetCameraMatrix();

		bool isInView(const glm::vec2& pos, const glm::vec2 size);

		float GetAspectRatio() const { return (float)scrnWidth / (float)scrnHeight; }
	private:
		glm::vec2 position;
		glm::mat4 cameraMatrix;
		glm::mat4 orthoMatrix;
		float scale;
		bool needsMatrixUpdate;
		int scrnWidth, scrnHeight;
	};

}