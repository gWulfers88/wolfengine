#include "SpriteBatch.h"

#include <algorithm>

namespace WolfEngineGL
{

	Glyph::Glyph(const glm::vec4& destRect, const glm::vec4& uvRect, GLuint TextureID, float Depth, const Color& color) :
		textureID(TextureID),
		depth(Depth)
	{
		topLeft.color = color;
		topLeft.SetPos(destRect.x, destRect.y + destRect.w);
		topLeft.SetUV(uvRect.x, uvRect.y + uvRect.w);

		bottomLeft.color = color;
		bottomLeft.SetPos(destRect.x, destRect.y);
		bottomLeft.SetUV(uvRect.x, uvRect.y);

		bottomRight.color = color;
		bottomRight.SetPos(destRect.x + destRect.z, destRect.y);
		bottomRight.SetUV(uvRect.x + uvRect.z, uvRect.y);

		topRight.color = color;
		topRight.SetPos(destRect.x + destRect.z, destRect.y + destRect.w);
		topRight.SetUV(uvRect.x + uvRect.z, uvRect.y + uvRect.w);
	}

	Glyph::Glyph(const glm::vec4& destRect, const glm::vec4& uvRect, GLuint TextureID, float Depth, const Color& color, float angle) :
		textureID(TextureID),
		depth(Depth)
	{
		glm::vec2 halfDims(destRect.z / 2.0f, destRect.w / 2.0f);

		glm::vec2 tl(-halfDims.x, halfDims.y);
		glm::vec2 bl(-halfDims.x, -halfDims.y);
		glm::vec2 br(halfDims.x, -halfDims.y);
		glm::vec2 tr(halfDims.x, halfDims.y);

		//rotate the points
		tl = RotatePoint(tl, angle) + halfDims;
		bl = RotatePoint(bl, angle) + halfDims;
		br = RotatePoint(br, angle) + halfDims;
		tr = RotatePoint(tr, angle) + halfDims;

		topLeft.color = color;
		topLeft.SetPos(destRect.x + tl.x, destRect.y + tl.y);
		topLeft.SetUV(uvRect.x, uvRect.y + uvRect.w);

		bottomLeft.color = color;
		bottomLeft.SetPos(destRect.x + bl.x, destRect.y + bl.y);
		bottomLeft.SetUV(uvRect.x, uvRect.y);

		bottomRight.color = color;
		bottomRight.SetPos(destRect.x + br.x, destRect.y + br.y);
		bottomRight.SetUV(uvRect.x + uvRect.z, uvRect.y);

		topRight.color = color;
		topRight.SetPos(destRect.x + tr.x, destRect.y + tr.y);
		topRight.SetUV(uvRect.x + uvRect.z, uvRect.y + uvRect.w);
	}

	glm::vec2 Glyph::RotatePoint(glm::vec2 pos, float angle)
	{
		glm::vec2 newV;
		newV.x = pos.x * cos(angle) - pos.y * sin(angle);
		newV.y = pos.x * sin(angle) + pos.y * cos(angle);

		return newV;
	}

	SpriteBatch::SpriteBatch()
		: vboID(0),
		vao(0)
	{
	}

	SpriteBatch::~SpriteBatch()
	{
		
	}

	void SpriteBatch::Init()
	{
		CreateVertexArray();
	}

	void SpriteBatch::CreateVertexArray()
	{
		if (vao == 0)
		{
			glGenVertexArrays(1, &vao);
		}
		glBindVertexArray(vao);

		if (vboID == 0)
		{
			glGenBuffers(1, &vboID);
		}
		glBindBuffer(GL_ARRAY_BUFFER, vboID);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		//position attrib pointer
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));

		//Color attrib pointer
		glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), (void*)offsetof(Vertex, color));

		//UV attribute pointer
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, uv));

		glBindVertexArray(0);
	}

	void SpriteBatch::Begin(GlyphSortType sortType)
	{
		curSortType = sortType;
		renderBatches.clear();
		
		glyphs.clear();
	}

	void SpriteBatch::End()
	{
		glyphPtrs.resize(glyphs.size());
		
		for (unsigned int i = 0; i < glyphs.size(); i++)
		{
			glyphPtrs[i] = &glyphs[i];
		}

		SortGlyphs();
		CreateRenderBatches();
	}

	void SpriteBatch::CreateRenderBatches()
	{
		std::vector<Vertex> verticies;
		verticies.resize(glyphs.size() * 6);

		if (glyphs.empty())
		{
			return;
		}

		int offset = 0;
		int currVert = 0;

		renderBatches.emplace_back(offset, 6, glyphPtrs[0]->textureID);
		verticies[currVert++] = glyphPtrs[0]->topLeft;
		verticies[currVert++] = glyphPtrs[0]->bottomLeft;
		verticies[currVert++] = glyphPtrs[0]->bottomRight;
		verticies[currVert++] = glyphPtrs[0]->bottomRight;
		verticies[currVert++] = glyphPtrs[0]->topRight;
		verticies[currVert++] = glyphPtrs[0]->topLeft;
		offset += 6;

		 //curr Glyph

		for (unsigned int cg = 1; cg < glyphs.size(); cg++)
		{
			if (glyphPtrs[cg]->textureID != glyphPtrs[cg - 1]->textureID)
			{
				renderBatches.emplace_back(offset, 6, glyphPtrs[cg]->textureID);
			}
			else
			{
				renderBatches.back().numVerticies += 6;
			}
			verticies[currVert++] = glyphPtrs[cg]->topLeft;
			verticies[currVert++] = glyphPtrs[cg]->bottomLeft;
			verticies[currVert++] = glyphPtrs[cg]->bottomRight;
			verticies[currVert++] = glyphPtrs[cg]->bottomRight;
			verticies[currVert++] = glyphPtrs[cg]->topRight;
			verticies[currVert++] = glyphPtrs[cg]->topLeft;
			offset += 6;
		}

		glBindBuffer(GL_ARRAY_BUFFER, vboID);
		//orphan buffer
		glBufferData(GL_ARRAY_BUFFER, verticies.size() * sizeof(Vertex), nullptr, GL_DYNAMIC_DRAW);
		//Upload data
		glBufferSubData(GL_ARRAY_BUFFER, 0, verticies.size() * sizeof(Vertex), verticies.data());
		//unbind
		glBindBuffer(GL_ARRAY_BUFFER, 0);

	}

	void SpriteBatch::Draw(const glm::vec4& destRect, const glm::vec4& uvRect, GLuint textureID, float depth, const Color& color)
	{
		glyphs.emplace_back(destRect, uvRect, textureID, depth, color);
	}

	void SpriteBatch::Draw(const glm::vec4& destRect, const glm::vec4& uvRect, GLuint textureID, float depth, const Color& color, float angle)
	{
		glyphs.emplace_back(destRect, uvRect, textureID, depth, color, angle);
	}
	
	void SpriteBatch::Draw(const glm::vec4& destRect, const glm::vec4& uvRect, GLuint textureID, float depth, const Color& color, const glm::vec2& dir)
	{
		const glm::vec2 right(1.0f, 0.0f);

		float angle = acos(glm::dot(right, dir));

		if (dir.y < 0.0f)
		{
			angle = -angle;
		}

		glyphs.emplace_back(destRect, uvRect, textureID, depth, color, angle);
	}

	void SpriteBatch::Render()
	{
		glBindVertexArray(vao);

		for (unsigned int i = 0; i < renderBatches.size(); i++)
		{
			glBindTexture(GL_TEXTURE_2D, renderBatches[i].texture);
			glDrawArrays(GL_TRIANGLES, renderBatches[i].offset, renderBatches[i].numVerticies);
		}

		glBindVertexArray(0);
	}

	void SpriteBatch::SortGlyphs()
	{
		switch (curSortType)
		{
		case GlyphSortType::BACK_TO_FRONT:
		{
			std::stable_sort(glyphPtrs.begin(), glyphPtrs.end(), CompBackToFront);
		}break;

		case GlyphSortType::FRONT_TO_BACK:
		{
			std::stable_sort(glyphPtrs.begin(), glyphPtrs.end(), CompFrontToBack);
		}break;

		case GlyphSortType::TEXTURE:
		{
			std::stable_sort(glyphPtrs.begin(), glyphPtrs.end(), CompTexture);
		}break;

		case GlyphSortType::NONE:
			break;
		}
	}

	bool SpriteBatch::CompFrontToBack(Glyph* a, Glyph* b)
	{
		return (a->depth < b->depth);
	}

	bool SpriteBatch::CompBackToFront(Glyph* a, Glyph* b)
	{
		return (a->depth > b->depth);
	}
	
	bool SpriteBatch::CompTexture(Glyph* a, Glyph* b)
	{
		return (a->textureID < b->textureID);
	}
}