#include "GLSLProgram.h"
#include "IOManager.h"
#include "Errors.h"

#include <fstream>
#include <vector>

namespace WolfEngineGL
{

	GLSLProgram::GLSLProgram()
		: vsID(0),
		fsID(0),
		programID(0),
		numAttributes(0)
	{

	}

	GLSLProgram::~GLSLProgram()
	{
		//We dont need the program anymore.
		glDeleteProgram(programID);
	}

	void GLSLProgram::CompileShaders(const std::string& vsFilePath, const std::string& fsFilePath)
	{
		std::string vertSource, fragSource;

		IOManager::ReadFileToBuffer(vsFilePath, vertSource);
		IOManager::ReadFileToBuffer(fsFilePath, fragSource);

		CompileShadersFromSource(vertSource.c_str(), fragSource.c_str());
	}

	void GLSLProgram::CompileShadersFromSource(const char* vertexSource, const char* fragmentSource)
	{
		//Vertex and fragment shaders are successfully compiled
		//now time to link them together into a program
		//Get a program object.
		programID = glCreateProgram();

		vsID = glCreateShader(GL_VERTEX_SHADER);

		if (vsID == 0)
		{
			fatalError("Vertex shader failed to be created!");
		}

		fsID = glCreateShader(GL_FRAGMENT_SHADER);

		if (fsID == 0)
		{
			fatalError("Fragment shader failed to be created!");
		}

		CompileShader(vertexSource, vsID, "VertexShader");
		CompileShader(fragmentSource, fsID, "FragmentShader");
	}

	void GLSLProgram::CompileShader(const char* source, GLuint shaderID, const std::string& shaderName)
	{
		glShaderSource(shaderID, 1, &source, nullptr);

		glCompileShader(shaderID);

		GLint Success = 0;

		glGetShaderiv(shaderID, GL_COMPILE_STATUS, &Success);

		if (Success == GL_FALSE)
		{
			GLint maxLn = 0;
			glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &maxLn);

			//Max length includes the null character
			std::vector<char> errorLog(maxLn);
			glGetShaderInfoLog(shaderID, maxLn, &maxLn, &errorLog[0]);

			//Provide the info log 
			//Exit with failure
			glDeleteShader(shaderID);

			std::printf("%s\n", &(errorLog[0]));
			fatalError("GLSL: " + shaderName + " Failed to compile!");
		}
	}

	//void GLSLProgram::CompileShader(const std::string& filePath, GLuint shaderID)
	//{
	//	std::ifstream vsFile(filePath);
	//	if (vsFile.fail())
	//	{
	//		perror(filePath.c_str());
	//		fatalError("Failed to open " + filePath);
	//	}

	//	std::string fileContent = "";

	//	std::string line;

	//	while (std::getline(vsFile, line))
	//	{
	//		fileContent += line + "\n";
	//	}

	//	vsFile.close();

	//	const char* contentsPtr = fileContent.c_str();

	//	glShaderSource(shaderID, 1, &contentsPtr, nullptr);

	//	glCompileShader(shaderID);

	//	GLint Success = 0;

	//	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &Success);

	//	if (Success == GL_FALSE)
	//	{
	//		GLint maxLn = 0;
	//		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &maxLn);

	//		//Max length includes the null character
	//		std::vector<char> errorLog(maxLn);
	//		glGetShaderInfoLog(shaderID, maxLn, &maxLn, &errorLog[0]);

	//		//Provide the info log 
	//		//Exit with failure
	//		glDeleteShader(shaderID);

	//		std::printf("%s\n", &(errorLog[0]));
	//		fatalError("GLSL: " + filePath + " Failed to compile!");
	//	}
	//}

	void GLSLProgram::LinkShader()
	{
		//Attach our shaders to our program
		glAttachShader(programID, vsID);
		glAttachShader(programID, fsID);

		//Link our program
		glLinkProgram(programID);

		//Note the different functions here: glGetProgram* instead of glGetShader*.
		GLint isLinked = 0;
		glGetProgramiv(programID, GL_LINK_STATUS, (int*)&isLinked);

		if (isLinked == GL_FALSE)
		{
			GLint maxLn = 0;
			glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &maxLn);

			//The max length includes the NULL character
			std::vector<char> errorLog(maxLn);
			glGetProgramInfoLog(programID, maxLn, &maxLn, &errorLog[0]);

			//We dont need the program anymore.
			glDeleteProgram(programID);

			//Dont leak shaders either
			glDeleteShader(vsID);
			glDeleteShader(fsID);

			//Use the infoLog as you see fit
			std::printf("%s\n", &(errorLog[0]));
			fatalError("Failed to link shaders!");
		}

		glDetachShader(programID, vsID);
		glDetachShader(programID, fsID);

		glDeleteShader(vsID);
		glDeleteShader(fsID);

		//We dont need the program anymore.
		//glDeleteProgram(programID);
	}

	void GLSLProgram::AddAttributes(const std::string& attributeName)
	{
		glBindAttribLocation(programID, numAttributes++, attributeName.c_str());
	}

	GLint GLSLProgram::GetUniformLoc(const std::string &uniformName)
	{
		GLint loc = glGetUniformLocation(programID, uniformName.c_str());

		if (loc == GL_INVALID_INDEX)
		{
			fatalError("Unifrom " + uniformName + " not found in shader!");
		}

		return loc;
	}

	void GLSLProgram::Use()
	{
		glUseProgram(programID);

		//glEnableVertexAttribArray(0);

		for (int i = 0; i < numAttributes; i++)
		{
			glEnableVertexAttribArray(i);
		}
	}

	void GLSLProgram::Unuse()
	{
		glUseProgram(0);

		//glDisableVertexAttribArray(0);

		for (int i = 0; i < numAttributes; i++)
		{
			glDisableVertexAttribArray(i);
		}
	}

	void GLSLProgram::Dispose()
	{
		if (programID != 0)
		{
			glDeleteProgram(programID);
		}
	}
}