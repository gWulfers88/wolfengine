#pragma once

#include "TextureCache.h"
#include <string>

namespace WolfEngineGL
{

	class ResourceManager
	{
	public:
		static GLTexture GetTexture(std::string texturePath);

	private:
		static TextureCache textureCache;
	};

}