#pragma once

#include <SDL/SDL.h>
#include <GL/glew.h>

#include <string>

namespace WolfEngineGL
{
	const int SCRN_WIDTH = 1024;
	const int SCRN_HEIGHT = 768;
	const std::string SCRN_NAME = "WolfEngine: ";

	enum WindowFlags
	{
		INVISIBLE = 0x1,
		FULLSCREEN = 0x2,
		BORDERLESS = 0x4,
	};

	class Window
	{
	public:
		Window();
		~Window();

		int CreateWindow(std::string windowName = SCRN_NAME, int screenWidth = SCRN_WIDTH, int screenHeight = SCRN_HEIGHT, unsigned int currentFlags = 0);

		void SwapBuffers();

		int GetScreenWidth();
		int GetScreenHeight();
		SDL_Window* GetWindow();

	private:
		SDL_Window* m_pWindow;
		int m_ScrnWidth, m_ScrnHeight;

	};

}