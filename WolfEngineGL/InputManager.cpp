#include "InputManager.h"

namespace WolfEngineGL
{

	InputManager::InputManager()
		: mouseCoords(0, 0)
	{
	}


	InputManager::~InputManager()
	{
	}

	void InputManager::Update()
	{
		for (auto iter : keyMap)
		{
			prevKeyMap[iter.first] = iter.second;
		}
	}

	void InputManager::KeyPressed(unsigned int KeyID)
	{
		keyMap[KeyID] = true;
	}

	void InputManager::KeyReleased(unsigned int keyID)
	{
		keyMap[keyID] = false;
	}

	bool InputManager::isKeyDown(unsigned int keyID)
	{
		auto it = keyMap.find(keyID);

		if (it != keyMap.end())
		{
			return it->second;
		}

		return false;
	}

	bool InputManager::isKeyPressed(unsigned int keyID)
	{
		if (isKeyDown(keyID) && !wasKeyDown(keyID))
		{
			return true;
		}

		return false;
	}

	bool InputManager::wasKeyDown(unsigned int keyID)
	{
		auto it = prevKeyMap.find(keyID);

		if (it != prevKeyMap.end())
		{
			return it->second;
		}

		return false;
	}

	void InputManager::SetMouseCoords(float x, float y)
	{
		mouseCoords.x = x;
		mouseCoords.y = y;
	}

	glm::vec2 InputManager::GetMouseCoords()
	{
		return mouseCoords;
	}
}
