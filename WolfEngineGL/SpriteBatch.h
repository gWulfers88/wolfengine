#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>

#include "Vertex.h"

namespace WolfEngineGL
{
	enum class GlyphSortType
	{
		NONE,
		FRONT_TO_BACK,
		BACK_TO_FRONT,
		TEXTURE,
	};

	class Glyph
	{
	public:
		Glyph();
		Glyph(const glm::vec4& destRect, const glm::vec4& uvRect, GLuint TextureID, float Depth, const Color& color);
		Glyph(const glm::vec4& destRect, const glm::vec4& uvRect, GLuint TextureID, float Depth, const Color& color, float angle);
		//Glyph(const glm::vec4& destRect, const glm::vec4& uvRect, GLuint TextureID, float Depth, const Color& color, const glm::vec2& dir);

		GLuint textureID;
		float depth;

		Vertex topLeft;
		Vertex bottomLeft;
		Vertex topRight;
		Vertex bottomRight;

	private:
		glm::vec2 RotatePoint(glm::vec2, float angle);
	};

	class RenderBatch
	{
	public:
		RenderBatch(GLuint Offset, GLuint NumVertices, GLuint Textures)
			: offset(Offset),
			numVerticies(NumVertices),
			texture(Textures)
		{ }

		GLuint offset;
		GLuint numVerticies;
		GLuint texture;
	};

	class SpriteBatch
	{
	public:
		SpriteBatch();
		~SpriteBatch();

		void Init();

		void Begin( GlyphSortType sortType = GlyphSortType::TEXTURE);
		void End();
		
		void Draw(const glm::vec4& destRect, const glm::vec4& uvRect, GLuint textureID, float depth, const Color& color);
		void Draw(const glm::vec4& destRect, const glm::vec4& uvRect, GLuint textureID, float depth, const Color& color, float angle);
		void Draw(const glm::vec4& destRect, const glm::vec4& uvRect, GLuint textureID, float depth, const Color& color, const glm::vec2& dir);

		void Render();

	private:
		void CreateRenderBatches();
		void CreateVertexArray();
		void SortGlyphs();

		static bool CompFrontToBack(Glyph* a, Glyph* b);
		static bool CompBackToFront(Glyph* a, Glyph* b);
		static bool CompTexture(Glyph* a, Glyph* b);

		GlyphSortType curSortType;
		GLuint vboID;
		GLuint vao;

		std::vector<Glyph*> glyphPtrs;	//sorting
		std::vector<Glyph> glyphs;	//actual glyphs

		std::vector<RenderBatch> renderBatches;
	};

}