#include "Errors.h"
#include <iostream>
#include <SDL/SDL.h>

namespace WolfEngineGL
{

	void fatalError(std::string errStr)
	{
		std::cout << errStr << std::endl;
		std::cout << "Enter any key to exit...";
		int tmp;
		std::cin >> tmp;
		SDL_Quit();
		exit(1);
	}

}