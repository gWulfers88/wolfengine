#pragma once

#include <functional>

#include <glm/glm.hpp>
#include "Vertex.h"
#include "SpriteBatch.h"
#include "GLTexture.h"

namespace WolfEngineGL
{

	class Particle2D
	{
	public:
		glm::vec2 m_position = glm::vec2(0);
		glm::vec2 m_velocity = glm::vec2(0);

		Color m_color;
		float m_life = 1.0f;
		bool m_isActive = false;
		float m_width = 0.0f;
	};

	inline void defUpdate(Particle2D& particle, float deltaTime)
	{
		particle.m_position += particle.m_velocity * deltaTime;
	}

	class ParticleBatch2D
	{
	public:
		ParticleBatch2D();
		~ParticleBatch2D();

		void Init(int maxParticles, float decayRate, GLTexture texture, std::function<void(Particle2D&, float)> updateFunc = defUpdate);

		void Update(float deltaTime);
		void Draw(SpriteBatch* spriteBatch);

		void AddParticle(const glm::vec2& pos, const glm::vec2 vel, const Color& color, float width);

	private:
		int FindFreeParticle();

		std::function<void(Particle2D&, float)> m_updateFunc;

		float m_decayRate = 0.1f;
		Particle2D* m_particles = nullptr;
		int m_maxParticles = 0;
		int m_lastFreeParticle = 0;
		GLTexture m_texture;
	};

}