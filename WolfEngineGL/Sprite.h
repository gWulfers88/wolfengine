#pragma once

#include <GL/glew.h>
#include "GLTexture.h"
#include <string>

namespace WolfEngineGL
{

	class Sprite
	{
	public:
		Sprite();
		~Sprite();

		void Init(float x, float y, float w, float h, std::string texturePath);

		void Draw();

	private:
		float m_x, m_y, m_w, m_h;
		GLuint m_vboID;
		GLTexture pTexture;
	};

}