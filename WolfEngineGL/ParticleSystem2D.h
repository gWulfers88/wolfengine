#pragma once

#include <vector>

namespace WolfEngineGL
{

	class ParticleBatch2D;
	class SpriteBatch;

	class ParticleSystem2D
	{
	public:
		ParticleSystem2D();
		~ParticleSystem2D();

		void AddParticleBatch(ParticleBatch2D* particleBatch);
		void Update(float deltaTime);
		void Draw(SpriteBatch* spriteBatch);

	protected:
		std::vector<ParticleBatch2D*> m_batches;
	};

}