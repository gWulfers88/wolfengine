#include "ParticleBatch2D.h"

namespace WolfEngineGL
{

	ParticleBatch2D::ParticleBatch2D()
	{
	}


	ParticleBatch2D::~ParticleBatch2D()
	{
		delete[] m_particles;
	}

	void ParticleBatch2D::Init(int maxParticles, float decayRate, GLTexture texture, std::function<void(Particle2D&, float)> updateFunc)
	{
		m_maxParticles = maxParticles;
		m_decayRate = decayRate;
		m_texture = texture;

		m_particles = new Particle2D[m_maxParticles];

		m_updateFunc = updateFunc;
	}

	void ParticleBatch2D::Update(float deltaTime)
	{
		for (int i = 0; i < m_maxParticles; i++)
		{
			if (m_particles[i].m_life > 0.0f)
			{
				m_updateFunc(m_particles[i], deltaTime);
				m_particles[i].m_life -= m_decayRate * deltaTime;
			}
		}
	}

	void ParticleBatch2D::Draw(SpriteBatch* spriteBatch)
	{
		glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);

		for (int i = 0; i < m_maxParticles; i++)
		{
			auto& p = m_particles[i];

			if (m_particles[i].m_life > 0.0f)
			{
				glm::vec4 destRect(p.m_position.x, p.m_position.y, p.m_width, p.m_width);

				spriteBatch->Draw(destRect, uvRect, m_texture.ID, 0.0f, p.m_color);
			}
		}
	}

	void ParticleBatch2D::AddParticle(const glm::vec2& pos, const glm::vec2 vel, const Color& color, float width)
	{
		int paricleIndex = FindFreeParticle();

		auto& p = m_particles[paricleIndex];

		p.m_life = 1.0f;
		p.m_position = pos;
		p.m_velocity = vel;
		p.m_color = color;
		p.m_width = width;
	}

	int ParticleBatch2D::FindFreeParticle()
	{
		for (int i = m_lastFreeParticle; i < m_maxParticles; i++)
		{
			if (m_particles[i].m_life <= 0.0f)
			{
				m_lastFreeParticle = i;
				return i;
			}
		}

		for (int i = 0; i < m_lastFreeParticle; i++)
		{
			if (m_particles[i].m_life <= 0.0f)
			{
				m_lastFreeParticle = i;
				return i;
			}
		}

		return 0;
	}
}