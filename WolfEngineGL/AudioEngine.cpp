#include "AudioEngine.h"
#include "Errors.h"

namespace WolfEngineGL
{
	void SoundEffect::Play( int loop )
	{
		if (Mix_PlayChannel(-1, m_chunk, loop) == -1)
		{
			if (Mix_PlayChannel(0, m_chunk, loop) == -1)
			{
				fatalError("Mix_PlayChannel() error: " + std::string(Mix_GetError()));
			}
		}
	}

	void Music::Play( int loops )
	{
		Mix_PlayMusic(m_music, loops);
	}

	void Music::Pause()
	{
		Mix_PauseMusic();
	}

	void Music::Stop()
	{
		Mix_HaltMusic();
	}
	
	void Music::Resume()
	{
		Mix_ResumeMusic();
	}

	AudioEngine::AudioEngine() :
		m_isInitialized(false)
	{
	}

	AudioEngine::~AudioEngine()
	{
		Destroy();
	}

	void AudioEngine::Init()
	{
		if (!m_isInitialized)
		{
			if (Mix_Init(MIX_INIT_MP3 | MIX_INIT_OGG) == -1)
			{
				fatalError("Mix_Init() error: " + std::string(Mix_GetError()));
			}

			if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 1024) == -1)
			{
				fatalError("Mix_OpenAudio() error: " + std::string(Mix_GetError()));
			}

			m_isInitialized = true;
		}
	}

	void AudioEngine::Destroy()
	{
		if (m_isInitialized)
		{
			m_isInitialized = false;

			for (auto& iter : m_effectMap)
			{
				Mix_FreeChunk(iter.second);
			}

			for (auto& iter : m_musicMap)
			{
				Mix_FreeMusic(iter.second);
			}

			m_effectMap.clear();
			m_musicMap.clear();

			Mix_CloseAudio();

			Mix_Quit();
		}
	}

	SoundEffect AudioEngine::LoadSFX(const std::string& filename)
	{
		auto iter = m_effectMap.find(filename);
		SoundEffect effect;

		if (iter == m_effectMap.end())
		{
			Mix_Chunk* chunk = Mix_LoadWAV(filename.c_str());

			if (chunk == nullptr)
			{
				fatalError("Mix_LoadWAV() error: " + std::string(Mix_GetError()));
			}

			effect.m_chunk = chunk;
			m_effectMap[filename] = chunk;
		}
		else
		{
			effect.m_chunk = iter->second;
		}

		return effect;
	}

	Music AudioEngine::LoadMusic(const std::string& filename)
	{
		auto iter = m_musicMap.find(filename);
		Music music;

		if (iter == m_musicMap.end())
		{
			Mix_Music* mixMusic = Mix_LoadMUS(filename.c_str());

			if (mixMusic == nullptr)
			{
				fatalError("Mix_LoadMUS() error: " + std::string(Mix_GetError()));
			}

			music.m_music = mixMusic;
			m_musicMap[filename] = mixMusic;
		}
		else
		{
			music.m_music = iter->second;
		}

		return music;
	}
}