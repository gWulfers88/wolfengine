#pragma once

#include "Vertex.h"
#include "GLSLProgram.h"

#include <glm/glm.hpp>

#include <vector>

namespace WolfEngineGL
{

	class DebugRenderer
	{
	public:

		struct DebugVertex
		{
			glm::vec2 position;
			Color color;
		};

		DebugRenderer();
		~DebugRenderer();

		void Init();

		void End();

		void DrawLine(const glm::vec2& a, const glm::vec2& b, const Color& color);
		void DrawBox(const glm::vec4& destRect, const Color& color, float angle);
		void DrawCircle(const glm::vec2& center, const Color& color, float radius);

		void Render(const glm::mat4& projMat, float lineWidth);

		void Dispose();

	private:
		GLSLProgram m_program;
		
		std::vector<DebugVertex> m_verices;
		std::vector<GLuint> m_indices;

		GLuint m_vbo, m_vao, m_ibo;

		int m_numElements = 0;
	};

}