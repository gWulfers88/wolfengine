#pragma once

#include <string>
#include <GL/glew.h>

namespace WolfEngineGL
{

	class GLSLProgram
	{
	public:
		GLSLProgram();
		~GLSLProgram();

		void CompileShadersFromSource(const char* vertexSource, const char* fragmentSource);
		void CompileShaders(const std::string& vsFilePath, const std::string& fsFilePath);

		void LinkShader();
		void AddAttributes(const std::string& attributeName);

		GLint GetUniformLoc(const std::string &uniformName);
		void Use();
		void Unuse();
		void Dispose();
	private:

		//void CompileShader(const std::string& filePath, GLuint shaderID);
		void CompileShader(const char* source, GLuint shaderID, const std::string& shaderName);

		int numAttributes;
		GLuint programID;
		GLuint vsID, fsID;

	};

}