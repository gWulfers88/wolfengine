#pragma once

#include <GL/glew.h>

namespace WolfEngineGL
{

	//8 bytes
	struct Position
	{
		float X;
		float Y;
	};

	//8 bit Color RGBA8
	struct Color
	{
		//Default Constructor
		Color(): R(0), G(0), B(0), A(0) {}
		//Overload Constructor
		Color(GLubyte r, GLubyte g, GLubyte b, GLubyte a) :
			R(r), G(g), B(b), A(a) {}

		//Red value 0 - 255
		GLubyte R;
		//Green value 0 - 255
		GLubyte G;
		//Blue value 0 - 255
		GLubyte B;
		//Alpha value 0 - 255
		GLubyte A;
	};

	struct UV
	{
		float U;
		float V;
	};

	//12 bytes
	struct Vertex
	{
		Position position;
		Color color;
		UV uv;

		void SetColor(GLubyte r, GLubyte g, GLubyte b, GLubyte a)
		{
			color.R = r;
			color.G = g;
			color.B = b;
			color.A = a;
		}

		void SetUV(float u, float v)
		{
			uv.U = u;
			uv.V = v;
		}

		void SetPos(float x, float y)
		{
			position.X = x;
			position.Y = y;
		}
	};

}