#pragma once

#include "IMainGame.h"

namespace WolfEngineGL
{

#define NO_SCREEN_INDEX -1

	enum class ScreenState
	{
		NONE,
		RUNNING,
		EXIT,
		CHANGE_NEXT,
		CHANGE_PREV
	};

	class IGameScreen
	{
	public:
		friend class ScreenList;

		IGameScreen();
		virtual ~IGameScreen();

		virtual void Build() = 0;
		virtual void Destroy() = 0;

		virtual void OnEntry() = 0;
		virtual void OnExit() = 0;

		virtual void OnUpdate() = 0;
		virtual void OnDraw() = 0;

		int GetIndex() { return m_Index; }
		
		ScreenState GetState() { return m_currState; }

		virtual int GetNextIndex() const = 0;
		virtual int GetPrevIndex() const = 0;

		void SetState(ScreenState newState){ m_currState = newState; }

		void SetParentGame(IMainGame* game) { m_game = game; }
	protected:
		ScreenState m_currState = ScreenState::NONE;
		int m_Index = NO_SCREEN_INDEX;
		IMainGame* m_game = nullptr;

	};

}