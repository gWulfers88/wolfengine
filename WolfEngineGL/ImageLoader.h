#pragma once

#include "GLTexture.h"
#include <string>

namespace WolfEngineGL
{

	class ImageLoader
	{
	public:
		static GLTexture LoadPNG(std::string filePath);
	};

}