
#pragma once

#include "GLTexture.h"

#include <glm/glm.hpp>

namespace WolfEngineGL
{
	class SpriteSheet
	{
	public:
		void Init(const GLTexture& texture, const glm::ivec2& tileSize)
		{
			this->m_texture = texture;
			this->m_tileSize = tileSize;

		}

		glm::vec4 GetUVs(int index)
		{
			int xTile = index % m_tileSize.x;
			int yTile = index / m_tileSize.x;

			glm::vec4 uvs;
			uvs.x = xTile / (float)m_tileSize.x;
			uvs.y = yTile / (float)m_tileSize.y;
			uvs.z = 1.0f / m_tileSize.x;
			uvs.w = 1.0f / m_tileSize.y;

			return uvs;
		}

		GLTexture m_texture;
		glm::ivec2 m_tileSize;
	};

}