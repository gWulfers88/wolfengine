#pragma once

#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>
#include <glm/glm.hpp>
#include <SDL/SDL_events.h>

namespace WolfEngineGL
{

	class GUI
	{
	public:
		GUI();
		~GUI();

		void Init(const std::string& resourceDir);

		void Update();

		void DrawGUI();

		void LoadScheme(const std::string& schemeFile);
		void SetFont(const std::string& fontFile);

		CEGUI::Window* CreateWidget(const std::string& type, const glm::vec4& destRectPercent, const glm::vec4& destRectPixel, const std::string& name = "");
		CEGUI::Window* CreateWidget(CEGUI::Window* parent, const std::string& type, const glm::vec4& destRectPercent, const glm::vec4& destRectPixel, const std::string& name = "");

		static void SetWidgetRect(CEGUI::Window* widget,const glm::vec4& destRectPercent, const glm::vec4& destRectPixel);

		void SetMouseCursor( const std::string& imageFile);
		void ShowMouseCursor();
		void HideMouseCursor();

		void OnSDLEvent(SDL_Event& evnt);

		void Destroy();

		static CEGUI::OpenGL3Renderer* GetRenderer() { return m_guiRenderer; }
		CEGUI::GUIContext* GetContext() { return m_context; }

	private:
		static CEGUI::OpenGL3Renderer* m_guiRenderer;
		CEGUI::GUIContext* m_context = nullptr;
		CEGUI::Window* m_root = nullptr;
		unsigned int m_lastTime = 0;

	};

}