#pragma once

#include <string>

namespace WolfEngineGL
{
	extern void fatalError(std::string errStr);
}