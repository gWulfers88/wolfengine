#pragma once

#include <vector>

namespace WolfEngineGL
{

	class IOManager
	{
	public:
		static bool ReadFileToBuffer(std::string filePath, std::vector<unsigned char>& buffer);
		static bool ReadFileToBuffer(std::string filePath, std::string& buffer);
	};

}
