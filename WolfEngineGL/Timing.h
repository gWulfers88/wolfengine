#pragma once

namespace WolfEngineGL
{
	class FPSLimiter
	{
	public:
		FPSLimiter();
		~FPSLimiter();

		void Init(float newTargetFPS);
		void SetTargetFPS(float newTargetFPS);
		void Start();
		float Stop();
		
		float GetTargetFPS() { return targetFPS; }

	private:
		void CalculateFPS();

		float targetFPS;
		float frameTime;
		float fps;

		unsigned int startTicks;
	};
}