#include "Camera2D.h"

namespace WolfEngineGL
{

	Camera2D::Camera2D()
		: position(0.0f, 0.0f),
		cameraMatrix(1.0f),
		orthoMatrix(0.0f),
		scale(1.0f),
		needsMatrixUpdate(true),
		scrnWidth(500),
		scrnHeight(500)
	{
	}

	Camera2D::~Camera2D()
	{
	}

	void Camera2D::Init(int screenWidth, int screenHeight)
	{
		scrnWidth = screenWidth;
		scrnHeight = screenHeight;
		orthoMatrix = glm::ortho(0.0f, (float)scrnWidth, 0.0f, (float)scrnHeight);
	}

	void Camera2D::Update()
	{
		if (needsMatrixUpdate)
		{
			//calculate camera matrix
			glm::vec3 translate(-position.x + scrnWidth / 2, -position.y + scrnHeight / 2, 0.0f);
			cameraMatrix = glm::translate(orthoMatrix, translate);

			glm::vec3 newScale(scale, scale, 0.0f);
			cameraMatrix = glm::scale(glm::mat4(1.0f), newScale) * cameraMatrix;

			needsMatrixUpdate = false;
		}
	}

	glm::vec2 Camera2D::GetWorldCoordsFromScreen(glm::vec2 screenCoords)
	{
		//invert y coord
		screenCoords.y = scrnHeight - screenCoords.y;

		//Make it so that 0 is center
		screenCoords -= glm::vec2(scrnWidth / 2, scrnHeight / 2);
		//scale the coords
		screenCoords /= scale;
		//translate with the camera pos
		screenCoords += position;

		return screenCoords;
	}

	void Camera2D::SetPosition(const glm::vec2& newPosition)
	{
		position = newPosition;
		needsMatrixUpdate = true;
	}

	glm::vec2 Camera2D::GetPosition()
	{
		return position;
	}

	void Camera2D::SetScale(float newScale)
	{
		scale = newScale;
		needsMatrixUpdate = true;
	}

	float Camera2D::GetScale()
	{
		return scale;
	}

	glm::mat4 Camera2D::GetCameraMatrix()
	{
		return cameraMatrix;
	}

	bool Camera2D::isInView(const glm::vec2& pos, const glm::vec2 size)
	{
		//AABB Col
		glm::vec2 scaledScrnDimension = glm::vec2( scrnWidth, scrnHeight) / (scale);

		const float MIN_DIST_X = size.x / 2.0f + scaledScrnDimension.x / 2.0f;
		const float MIN_DIST_Y = size.y / 2.0f + scaledScrnDimension.y / 2.0f;

		glm::vec2 centerPos = pos + size / 2.0f;
		glm::vec2 centerCamPos = position;
		glm::vec2 distVec = centerPos - centerCamPos;

		float xdepth = MIN_DIST_X - abs(distVec.x);
		float ydepth = MIN_DIST_Y - abs(distVec.y);

		if (xdepth > 0 && ydepth > 0)
		{
			//Collision
			return true;
		}

		return false;
	}
}