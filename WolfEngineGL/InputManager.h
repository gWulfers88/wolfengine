#pragma once

#include <glm/glm.hpp>

#include <unordered_map>

namespace WolfEngineGL
{

	class InputManager
	{
	public:
		InputManager();
		~InputManager();

		void Update();

		void KeyPressed(unsigned int KeyID);
		void KeyReleased(unsigned int keyID);
		
		bool isKeyDown(unsigned int keyID);
		bool isKeyPressed(unsigned int keyID);
		
		void SetMouseCoords(float x, float y);

		glm::vec2 GetMouseCoords();

	private:
		bool wasKeyDown(unsigned int keyID);

		std::unordered_map<unsigned int, bool> keyMap;
		std::unordered_map<unsigned int, bool> prevKeyMap;

		glm::vec2 mouseCoords;

	};

}