#pragma once

#include <SDL/SDL_mixer.h>

#include <string>
#include <map>

namespace WolfEngineGL
{

	class SoundEffect
	{
	public:
		friend class AudioEngine;

		void Play( int loop = 0 );

	private:
		Mix_Chunk* m_chunk = nullptr;

	};

	class Music
	{
	public:
		friend class AudioEngine;

		void Play( int loops = -1 );
		void Pause();
		void Stop();
		void Resume();

	private:
		Mix_Music* m_music = nullptr;
	};

	class AudioEngine
	{
	public:
		AudioEngine();
		~AudioEngine();

		void Init();
		void Destroy();

		SoundEffect LoadSFX(const std::string& filename);
		Music LoadMusic(const std::string& filename);

	private:
		std::map <std::string, Mix_Chunk*> m_effectMap;
		std::map <std::string, Mix_Music*> m_musicMap;

		bool m_isInitialized;
	};

}