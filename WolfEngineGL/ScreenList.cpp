#include "ScreenList.h"

#include "IGameScreen.h"
#include "IMainGame.h"

namespace WolfEngineGL
{

	ScreenList::ScreenList(IMainGame* game)
		: m_game(game),
		m_currScrnIndex(NO_SCREEN_INDEX)
	{

	}


	ScreenList::~ScreenList()
	{
		Destroy();
	}

	IGameScreen* ScreenList::MoveNext()
	{
		IGameScreen* currScreen = GetCurrent();

		if (currScreen->GetNextIndex() != NO_SCREEN_INDEX)
		{
			m_currScrnIndex = currScreen->GetNextIndex();
		}

		return GetCurrent();
	}

	IGameScreen* ScreenList::MovePrev()
	{
		IGameScreen* currScreen = GetCurrent();

		if (currScreen->GetPrevIndex() != NO_SCREEN_INDEX)
		{
			m_currScrnIndex = currScreen->GetPrevIndex();
		}

		return GetCurrent();
	}

	IGameScreen* ScreenList::GetCurrent()
	{
		if (m_currScrnIndex == NO_SCREEN_INDEX)
		{
			return nullptr;
		}

		return m_screens[m_currScrnIndex];
	}

	void ScreenList::SetScreen(int nextScreen)
	{
		m_currScrnIndex = nextScreen;
	}

	void ScreenList::AddScreen(IGameScreen* newScreen)
	{
		newScreen->m_Index = m_screens.size();
		m_screens.push_back(newScreen);

		newScreen->Build();
		newScreen->SetParentGame(m_game);
	}

	void ScreenList::Destroy()
	{
		for (size_t i = 0; i < m_screens.size(); i++)
		{
			m_screens[i]->Destroy();
		}

		m_screens.resize(0);
		m_currScrnIndex = NO_SCREEN_INDEX;
	}

	
}