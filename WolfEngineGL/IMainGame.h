#pragma once

#include "WolfEngineGL.h"
#include "Window.h"
#include "InputManager.h"

#include <memory>

namespace WolfEngineGL
{
	class ScreenList;
	class IGameScreen;

	class IMainGame
	{
	public:
		IMainGame();
		virtual ~IMainGame();

		void Run();

		void Exit();

		virtual void OnInit() = 0;
		virtual void AddScreens() = 0;
		virtual void OnExit() = 0;

		const float GetFPS() { return m_fps; }

		void OnSDLEvent(SDL_Event& evnt);

		InputManager m_inputManager;

	protected:
		virtual void Update();
		virtual void Draw();

		bool Init();
		bool InitSystems();

		std::unique_ptr<ScreenList> m_screenList = nullptr;
		IGameScreen* m_currentScreen = nullptr;
		bool m_isRunning = false;
		float m_fps = 0;
		Window m_window;
		
	};

}
