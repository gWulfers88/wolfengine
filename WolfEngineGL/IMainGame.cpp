#include "IMainGame.h"
#include "Timing.h"

#include "ScreenList.h"
#include "IGameScreen.h"

namespace WolfEngineGL
{

	IMainGame::IMainGame()
	{
		m_screenList = std::make_unique<ScreenList>(this);
	}

	IMainGame::~IMainGame()
	{

	}

	void IMainGame::Run()
	{
		if (Init())
		{
			FPSLimiter limiter;
			limiter.SetTargetFPS(60.0f);

			m_isRunning = true;

			while (m_isRunning)
			{
				limiter.Start();

				m_inputManager.Update();

				Update();

				if (m_isRunning)
				{
					Draw();

					m_fps = limiter.Stop();
					m_window.SwapBuffers();
				}
			}
		}
	}
	
	void IMainGame::Update()
	{
		if (m_currentScreen)
		{
			switch (m_currentScreen->GetState())
			{
			case ScreenState::RUNNING:
			{
				m_currentScreen->OnUpdate();
			}break;

			case ScreenState::CHANGE_NEXT:
			{
				m_currentScreen->OnExit();
				m_currentScreen = m_screenList->MoveNext();
				if (m_currentScreen)
				{
					m_currentScreen->SetState(ScreenState::RUNNING);
					m_currentScreen->OnEntry();
				}
			}break;

			case ScreenState::CHANGE_PREV:
			{
				m_currentScreen->OnExit();
				m_currentScreen = m_screenList->MovePrev();
				if (m_currentScreen)
				{
					m_currentScreen->SetState(ScreenState::RUNNING);
					m_currentScreen->OnEntry();
				}
			}break;

			case ScreenState::EXIT:
			{
				Exit();
			}break;

			default:
			{

			}break;

			}
		}
		else
		{
			Exit();
		}
	}

	void IMainGame::Draw()
	{
		glViewport(0, 0, m_window.GetScreenWidth(), m_window.GetScreenHeight());

		if (m_currentScreen && m_currentScreen->GetState() == ScreenState::RUNNING)
		{
			m_currentScreen->OnDraw();
		}
	}

	void IMainGame::Exit()
	{
		m_currentScreen->OnExit();

		if (m_screenList)
		{
			m_screenList->Destroy();
		}

		m_isRunning = false;
	}

	bool IMainGame::Init()
	{
		WolfEngineGL::Init();
		
		if (!InitSystems()) return false;

		OnInit();
		
		AddScreens();

		m_currentScreen = m_screenList->GetCurrent();
		m_currentScreen->OnEntry();
		m_currentScreen->SetState(ScreenState::RUNNING);

		return true;
	}

	bool IMainGame::InitSystems()
	{
		if (m_window.CreateWindow("Game") == 0)
		{
			return true;
		}

		return false;
	}

	void IMainGame::OnSDLEvent(SDL_Event& evnt)
	{
		switch (evnt.type)
		{
		case SDL_QUIT:
			Exit();
			break;

		case SDL_MOUSEMOTION:
		{
			m_inputManager.SetMouseCoords((float)evnt.motion.x, (float)evnt.motion.y);
		}break;

		case SDL_KEYDOWN:
			m_inputManager.KeyPressed(evnt.key.keysym.sym);
			break;

		case SDL_KEYUP:
			m_inputManager.KeyReleased(evnt.key.keysym.sym);
			break;

		case SDL_MOUSEBUTTONDOWN:
			m_inputManager.KeyPressed(evnt.button.button);
			break;

		case SDL_MOUSEBUTTONUP:
			m_inputManager.KeyReleased(evnt.button.button);
			break;
		}
	}
}
