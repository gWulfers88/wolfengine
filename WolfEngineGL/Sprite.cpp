#include "Sprite.h"
#include "Vertex.h"
#include "ResourceManager.h"

#include <iostream>
#include <cstddef>

namespace WolfEngineGL
{

	Sprite::Sprite()
		: m_vboID(0)
	{

	}

	Sprite::~Sprite()
	{
		std::cout << "deleting sprite.." << std::endl;

		if (m_vboID != 0)
		{
			glDeleteBuffers(1, &m_vboID);
		}
	}

	void Sprite::Init(float x, float y, float w, float h, std::string texturePath)
	{
		m_x = x;
		m_y = y;
		m_w = w;
		m_h = h;

		pTexture = ResourceManager::GetTexture(texturePath);

		if (m_vboID == 0)
		{
			glGenBuffers(1, &m_vboID);
		}

		Vertex vertextData[6];

		//First Triangle
		vertextData[0].SetPos(m_x + m_w, m_y + m_h);
		vertextData[0].SetUV(1, 1);

		vertextData[1].SetPos(m_x, m_y + m_h);
		vertextData[1].SetUV(0.0f, 1.0f);

		vertextData[2].SetPos(m_x, m_y);
		vertextData[2].SetUV(0.0f, 0.0f);

		//Second Triangle
		vertextData[3].SetPos(m_x, m_y);
		vertextData[3].SetUV(0.0f, 0.0f);

		vertextData[4].SetPos(m_x + m_w, m_y);
		vertextData[4].SetUV(1.0f, 0.0f);

		vertextData[5].SetPos(m_x + m_w, m_y + m_h);
		vertextData[5].SetUV(1, 1);

		for (int i = 0; i < 6; i++)
		{
			vertextData[i].SetColor(255, 0, 255, 255);
		}

		vertextData[1].SetColor(0, 255, 0, 255);
		vertextData[4].SetColor(0, 0, 255, 255);

		glBindBuffer(GL_ARRAY_BUFFER, m_vboID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertextData), vertextData, GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void Sprite::Draw()
	{
		glBindTexture(GL_TEXTURE_2D, pTexture.ID);

		glBindBuffer(GL_ARRAY_BUFFER, m_vboID);

		
		//Draw dtaa
		glDrawArrays(GL_TRIANGLES, 0, 6);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

}