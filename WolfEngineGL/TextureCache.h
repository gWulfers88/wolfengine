#pragma once

#include <map>
#include "GLTexture.h"

namespace WolfEngineGL
{

	class TextureCache
	{
	public:
		TextureCache();
		~TextureCache();

		GLTexture GetTexture(std::string texturePath);

	private:
		std::map<std::string, GLTexture> textureMap;
	};

}