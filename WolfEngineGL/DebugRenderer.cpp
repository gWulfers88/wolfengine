#include "DebugRenderer.h"

namespace
{
	const char* VERT_SRC = R"(#version 130
//The vertex shader operates on each vertex

//input data from the VBO. Each vertex is 2 floats
in vec2 vertexPosition;
in vec4 vertexColor;

out vec2 fragmentPosition;
out vec4 fragmentColor;

uniform mat4 P;

void main()
{
	//Set the X, Y position on the screen
	gl_Position.xy = (P * vec4(vertexPosition, 0.0, 1.0)).xy;
	
	//the Z position is zero since we are in 2D
	gl_Position.z = 0.0;
	
	//Indicate that the coords are normalized
	gl_Position.w = 1.0f;
	
	fragmentPosition = vertexPosition;
	fragmentColor = vertexColor;
})";

	const char* FRAG_SRC = R"(#version 130
//Fragment shader operates on each pixel in a given poly

in vec2 fragmentPosition;
in vec4 fragmentColor;

//This is the 3 component float vector that gets ouputted to the screen
//foreach pixel
out vec4 color;

void main()
{
	color = fragmentColor;
})";
}

namespace WolfEngineGL
{
	DebugRenderer::DebugRenderer()
		: m_vbo(0),
		m_vao(0),
		m_ibo(0)
	{

	}

	DebugRenderer::~DebugRenderer()
	{
		Dispose();
	}

	void DebugRenderer::Init()
	{
		//Shader Init
		m_program.CompileShadersFromSource(VERT_SRC, FRAG_SRC);
		m_program.AddAttributes("vertexPosition");
		m_program.AddAttributes("vertexColor");
		m_program.LinkShader();

		//Setup buffers
		glGenVertexArrays(1, &m_vao);
		glGenBuffers(1, &m_vbo);
		glGenBuffers(1, &m_ibo);

		//Bind 
		glBindVertexArray(m_vao);
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);

		//Vertex attrib pointers
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(DebugVertex), (void*)offsetof(DebugVertex, position));
		
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(DebugVertex), (void*)offsetof(DebugVertex, color));

		glBindVertexArray(0);
	}

	void DebugRenderer::End()
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
		//Orphan buffer
		glBufferData(GL_ARRAY_BUFFER, m_verices.size() * sizeof(DebugVertex), nullptr, GL_DYNAMIC_DRAW);
		//Upload
		glBufferSubData(GL_ARRAY_BUFFER, 0, m_verices.size() * sizeof(DebugVertex), m_verices.data());
		//Unbind
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
		//Orphan buffer
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(GLuint), nullptr, GL_DYNAMIC_DRAW);
		//Upload
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, m_indices.size() * sizeof(GLuint), m_indices.data());
		//Unbind
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		m_numElements = m_indices.size();
		m_indices.clear();
		m_verices.clear();
	}

	glm::vec2 rotatePoint(const glm::vec2& pos, float angle)
	{
		glm::vec2 newVector;
		newVector.x = pos.x * cos(angle) - pos.y * sin(angle);
		newVector.y = pos.x * sin(angle) + pos.y * cos(angle);
		return newVector;
	}

	void DebugRenderer::DrawLine(const glm::vec2& a, const glm::vec2& b, const Color& color)
	{
		int i = m_verices.size();
		m_verices.resize(m_verices.size() + 2);
		
		m_verices[i].position = a;
		m_verices[i].color = color;

		m_verices[i + 1].position = b;
		m_verices[i + 1].color = color;

		m_indices.push_back(i);
		m_indices.push_back(i + 1);
	}

	void DebugRenderer::DrawBox(const glm::vec4& destRect, const Color& color, float angle)
	{
		int i = m_verices.size();

		m_verices.resize(m_verices.size() + 4);

		glm::vec2 halfDims(destRect.z / 2.0f, destRect.w / 2.0f);

		//Get points centered at origin
		glm::vec2 tl(-halfDims.x, halfDims.y);
		glm::vec2 bl(-halfDims.x, -halfDims.y);
		glm::vec2 br(halfDims.x, -halfDims.y);
		glm::vec2 tr(halfDims.x, halfDims.y);

		glm::vec2 posOffset(destRect.x, destRect.y);

		//Rotate the points
		m_verices[i].position = rotatePoint(tl, angle) + halfDims + posOffset;
		m_verices[i + 1].position = rotatePoint(bl, angle) + halfDims + posOffset;
		m_verices[i + 2].position = rotatePoint(br, angle) + halfDims + posOffset;
		m_verices[i + 3].position = rotatePoint(tr, angle) + halfDims + posOffset;

		for (int j = i; j < i + 4; j++)
		{
			m_verices[j].color = color;
		}

		m_indices.reserve(m_indices.size() + 8);

		m_indices.push_back(i);
		m_indices.push_back(i + 1);

		m_indices.push_back(i + 1);
		m_indices.push_back(i + 2);
		
		m_indices.push_back(i + 2);
		m_indices.push_back(i + 3);

		m_indices.push_back(i + 3);
		m_indices.push_back(i);
	}
	
	void DebugRenderer::DrawCircle(const glm::vec2& center, const Color& color, float radius)
	{
		static const int NUM_VERTS = 100;

		int start = m_verices.size();

		m_verices.resize(m_verices.size() + NUM_VERTS);

		for (int i = 0; i < NUM_VERTS; i++)
		{
			float angle = ((float)i / NUM_VERTS) * 3.14159265359f * 2.0f;

			m_verices[start + i].position.x = cos(angle) * radius + center.x;
			m_verices[start + i].position.y = sin(angle) * radius + center.y;

			m_verices[start + i].color = color;
		}

		m_indices.reserve(m_indices.size() + NUM_VERTS * 2.0f);

		for (int i = 0; i < NUM_VERTS - 1; i++)
		{
			m_indices.push_back(start + i);
			m_indices.push_back(start + i + 1);
		}

		m_indices.push_back(start + NUM_VERTS - 1);
		m_indices.push_back(start);
	}

	void DebugRenderer::Render(const glm::mat4& projMat, float lineWidth)
	{
		m_program.Use();

		GLint pUniform = m_program.GetUniformLoc("P");
		glUniformMatrix4fv(pUniform, 1, GL_FALSE, &projMat[0][0]);

		glLineWidth(lineWidth);

		glBindVertexArray(m_vao);

		glDrawElements(GL_LINES, m_numElements, GL_UNSIGNED_INT, 0);

		glBindVertexArray(0);

		m_program.Unuse();
	}

	void DebugRenderer::Dispose()
	{
		if (m_vao)
		{
			glDeleteVertexArrays(1, &m_vao);
		}
		if (m_vbo)
		{
			glDeleteBuffers(1, &m_vbo);
		}
		if (m_ibo)
		{
			glDeleteBuffers(1, &m_ibo);
		}

		m_program.Dispose();
	}
}