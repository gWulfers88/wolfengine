#pragma once

#include <WolfEngineGL/IGameScreen.h>
#include <WolfEngineGL/Camera2D.h>
#include <WolfEngineGL/GUI.h>
#include <WolfEngineGL/DebugRenderer.h>
#include <WolfEngineGL/InputManager.h>

enum class ObjectType
{
	NO_COLLISION,
	COLLISION
};

enum class ObjectMode
{
	PLAYER,
	TILE,
};

enum class SelectMode
{
	DRAW,
	ERASE
};

#define WALL_TILE 'w'
#define GRASS_TILE 'g'
#define FLOOR_TILE 'f'
#define EMPTY_TILE '.'

#define SELECTION_NONE -1
#define GROUP_ID 1

class WidgetLabel
{
public:
	WidgetLabel(){}

};

class MainScreen :	public WolfEngineGL::IGameScreen
{
public:
	MainScreen( WolfEngineGL::Window* window );
	virtual ~MainScreen();

	virtual void Build() override;
	virtual void Destroy() override;
	virtual void OnEntry() override;
	virtual void OnExit() override;
	virtual int GetNextIndex() const override;
	virtual int GetPrevIndex() const override;
	virtual void OnUpdate() override;
	virtual void OnDraw() override;

private:
	void ProcessInput();
	void InitGUI();

	void UpdateMousePosition(const SDL_Event& evnt);
	void UpdateMouseDown(const SDL_Event& evnt);
	void UpdateMouseUp(const SDL_Event& evnt);

	// GUI EVENT HANDLERS
	bool OnExitClicked(const CEGUI::EventArgs& e);
	bool OnLvlWidthValueChange(const CEGUI::EventArgs& e);
	bool OnLvlHeightValueChange(const CEGUI::EventArgs& e);
	bool OnGenWolrdClicked(const CEGUI::EventArgs& e);
	bool OnDrawModeClicked(const CEGUI::EventArgs& e);
	bool OnEraseModeClicked(const CEGUI::EventArgs& e);

	bool needsGen = false;

	ObjectMode m_objectMode = ObjectMode::TILE;
	ObjectType m_objectType = ObjectType::NO_COLLISION;
	SelectMode m_selectMode = SelectMode::DRAW;

	CEGUI::GroupBox* groupBox = nullptr;
	CEGUI::PushButton* btnGenWorld = nullptr;
	CEGUI::Spinner* lvlWidthSpinner = nullptr;
	CEGUI::Spinner* lvlHeightSpinner = nullptr;
	CEGUI::RadioButton* m_drawModeButton = nullptr;
	CEGUI::RadioButton* m_eraseModeButton = nullptr;

	WolfEngineGL::Window* m_window;
	WolfEngineGL::Camera2D m_mainCamera;
	WolfEngineGL::Camera2D m_guiCamera;
	WolfEngineGL::GUI m_gui;
	WolfEngineGL::DebugRenderer m_outlineRenderer;
	WolfEngineGL::InputManager m_inputManager;

	std::vector<std::string> m_levelData;
};

