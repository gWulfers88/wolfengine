#include "Editor.h"

#include <WolfEngineGL/ScreenList.h>

Editor::Editor()
{
}


Editor::~Editor()
{
}

void Editor::OnInit()
{

}

void Editor::AddScreens()
{
	m_mainScreen = std::make_unique<MainScreen>(&m_window);

	m_screenList->AddScreen(m_mainScreen.get());

	m_screenList->SetScreen(m_mainScreen->GetIndex());
}

void Editor::OnExit()
{

}