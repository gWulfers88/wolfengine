#pragma once

#include <WolfEngineGL\IMainGame.h>
#include "MainScreen.h"

class Editor :	public WolfEngineGL::IMainGame
{
public:
	Editor();
	virtual ~Editor();

	void OnInit() override;
	void AddScreens() override;
	void OnExit() override;

private:
	std::unique_ptr<MainScreen> m_mainScreen = nullptr;
};

