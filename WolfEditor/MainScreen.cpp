#include "MainScreen.h"

#include "Screens.h"

#include <SDL/SDL.h>

int lvlWidth = 0;
int lvlHeight = 0;

MainScreen::MainScreen( WolfEngineGL::Window* window)
	: m_window(window)
{
	m_Index = SCREEN_INDEX_MAIN;
}

MainScreen::~MainScreen()
{

}

void MainScreen::Build()
{

}

void MainScreen::Destroy()
{

}

void MainScreen::OnEntry()
{
	m_mainCamera.Init(m_window->GetScreenWidth(), m_window->GetScreenHeight());
	m_mainCamera.SetScale(32.0f);

	m_guiCamera.Init(m_window->GetScreenWidth(), m_window->GetScreenHeight());
	m_guiCamera.SetScale(1.0f);

	m_outlineRenderer.Init();

	InitGUI();
}

void MainScreen::OnExit()
{
	m_gui.Destroy();
	m_outlineRenderer.Dispose();
}

int MainScreen::GetNextIndex() const
{
	return NO_SCREEN_INDEX;
}

int MainScreen::GetPrevIndex() const
{
	return NO_SCREEN_INDEX;
}

void MainScreen::OnUpdate()
{
	ProcessInput();

	m_mainCamera.Update();

	if (m_inputManager.isKeyDown(SDLK_d))
	{
		m_mainCamera.SetPosition(glm::vec2(m_mainCamera.GetPosition().x + 0.5f, m_mainCamera.GetPosition().y));
	}
	else if(m_inputManager.isKeyDown(SDLK_a))
	{
		m_mainCamera.SetPosition(glm::vec2(m_mainCamera.GetPosition().x - 0.5f, m_mainCamera.GetPosition().y));
	}

	if (m_inputManager.isKeyDown(SDLK_w))
	{
		m_mainCamera.SetPosition(glm::vec2(m_mainCamera.GetPosition().x, m_mainCamera.GetPosition().y + 0.5f));
	}
	else if (m_inputManager.isKeyDown(SDLK_s))
	{
		m_mainCamera.SetPosition(glm::vec2(m_mainCamera.GetPosition().x, m_mainCamera.GetPosition().y - 0.5f));
	}

	if (needsGen && !btnGenWorld->isActive())
	{
		btnGenWorld->enable();
		std::printf("DEBUG: world has changed and needs to be generated\n");
			
	}
}

void MainScreen::OnDraw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	{//Render Axis

		//Y-AXIS
		m_outlineRenderer.DrawLine(glm::vec2(0.0f), glm::vec2(0.0f, 1000.0f), WolfEngineGL::Color(0, 255, 0, 255));
		m_outlineRenderer.DrawLine(glm::vec2(0.0f), glm::vec2(0.0f, -1000.0f), WolfEngineGL::Color(0, 255, 0, 255));

		//X-AXIS
		m_outlineRenderer.DrawLine(glm::vec2(0.0f), glm::vec2(1000.0f, 0.0f), WolfEngineGL::Color(255, 0, 0, 255));
		m_outlineRenderer.DrawLine(glm::vec2(0.0f), glm::vec2(-1000.0f, 0.0f), WolfEngineGL::Color(255, 0, 0, 255));

		m_outlineRenderer.End();
		m_outlineRenderer.Render(m_mainCamera.GetCameraMatrix(), 2.0f);
	}

	{//Render world tile outline.
		int TILE_WIDTH = 2;

		for (int y = 0; y < m_levelData.size(); y++)
		{
			for (int x = 0; x < m_levelData[y].size(); x++)
			{
				char tile = m_levelData[y][x];

				glm::vec4 destRect;
				destRect.x = x * TILE_WIDTH;
				destRect.y = y * TILE_WIDTH;
				destRect.z = TILE_WIDTH;
				destRect.w = TILE_WIDTH;

				if (m_mainCamera.isInView(glm::vec2(destRect.x, destRect.y), glm::vec2(destRect.z, destRect.w)))
				{
					switch (tile)
					{
					case EMPTY_TILE:
					{
						m_outlineRenderer.DrawBox(destRect, WolfEngineGL::Color(255, 255, 255, 255), 0.0f);
						m_outlineRenderer.End();
						m_outlineRenderer.Render(m_mainCamera.GetCameraMatrix(), 2.0f);
					}break;

					case WALL_TILE:
					{
					
					}break;

					case GRASS_TILE:
					{
					
					}break;

					case FLOOR_TILE:
					{
					
					}break;

					default:
						printf("invalid token at ( %d, %d)", x, y);
						break;
					}
				}
			}
		}
	}

	m_gui.DrawGUI();
}

void MainScreen::ProcessInput()
{
	SDL_Event evnt;

	m_inputManager.Update();

	while(SDL_PollEvent(&evnt))
	{
		m_gui.OnSDLEvent(evnt);

		switch (evnt.type)
		{
		case SDL_QUIT:
		{
			OnExitClicked(CEGUI::EventArgs());
		}break;
		
		case SDL_MOUSEBUTTONDOWN:
		{
			UpdateMouseDown(evnt);
		}break;

		case SDL_MOUSEBUTTONUP:
		{
			UpdateMouseUp(evnt);
		}break;

		case SDL_MOUSEMOTION:
		{
			UpdateMousePosition(evnt);
		}break;

		case SDL_KEYDOWN:
		{
			m_inputManager.KeyPressed(evnt.key.keysym.sym);
		}break;

		case SDL_KEYUP:
		{
			m_inputManager.KeyReleased(evnt.key.keysym.sym);
		}break;

		case SDL_MOUSEWHEEL:
		{// Linear scaling sucks for mouse wheel zoom so we multiply by getScale() instead.
			m_mainCamera.OffsetScale(m_mainCamera.GetScale() * evnt.wheel.y * 0.1f);
		}break;

		}
	}
}

void MainScreen::InitGUI()
{
	m_gui.Init("GUI");

	m_gui.LoadScheme("TaharezLook.scheme");
	m_gui.SetFont("DejaVuSans-10");

	{//GROUP BOX
		float XPOS = 0.75f;
		float YPOS = 0.0f;
		float XDIM = 0.25f;
		float YDIM = 1.0f;

		groupBox = static_cast<CEGUI::GroupBox*>(m_gui.CreateWidget("TaharezLook/GroupBox", glm::vec4(XPOS, YPOS, XDIM, YDIM), glm::vec4(0.0f), "ToolBox"));
		groupBox->appendText("Tool Box");
	}

	{//EXIT BUTTON
		float YPOS = 0.95f;
		float XDIM = 1.0f;
		float YDIM = 0.05f;

		CEGUI::PushButton* btnExit = static_cast<CEGUI::PushButton*>(m_gui.CreateWidget(groupBox, "TaharezLook/Button", glm::vec4(0.0f, YPOS, XDIM, YDIM), glm::vec4(0.0f), "ExitButton"));
		btnExit->setText("Exit");

		btnExit->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MainScreen::OnExitClicked, this));
	}

	{	//Create Level Dimension Spinners and a Generate Level Button.
		float XPOS = 0.01f;
		float XDIM = 0.3f;
		float YDIM = 0.025f;
		float Offset = 0.25f;

		lvlWidthSpinner = static_cast<CEGUI::Spinner*>(m_gui.CreateWidget(groupBox, "TaharezLook/Spinner", glm::vec4(XPOS, XPOS, XDIM, YDIM), glm::vec4(0.0f), "LevelWidthSpinner"));
		lvlWidthSpinner->setMinimumValue(1.0);
		lvlWidthSpinner->setCurrentValue(5.0);
		lvlWidthSpinner->setMaximumValue(100.0);
		lvlWidthSpinner->setStepSize(1.0);
		lvlWidthSpinner->setTextInputMode(CEGUI::Spinner::Integer);

		lvlWidthSpinner->subscribeEvent(CEGUI::Spinner::EventValueChanged, CEGUI::Event::Subscriber(&MainScreen::OnLvlWidthValueChange, this));

		lvlHeightSpinner = static_cast<CEGUI::Spinner*>(m_gui.CreateWidget(groupBox, "TaharezLook/Spinner", glm::vec4(XDIM + Offset, XPOS, XDIM, YDIM), glm::vec4(0.0f), "LevelHeightSpinner"));
		lvlHeightSpinner->setMinimumValue(1.0);
		lvlHeightSpinner->setCurrentValue(5.0);
		lvlHeightSpinner->setMaximumValue(100.0);
		lvlHeightSpinner->setStepSize(1.0);
		lvlHeightSpinner->setTextInputMode(CEGUI::Spinner::Integer);

		lvlHeightSpinner->subscribeEvent(CEGUI::Spinner::EventValueChanged, CEGUI::Event::Subscriber(&MainScreen::OnLvlHeightValueChange, this));

		btnGenWorld = static_cast<CEGUI::PushButton*>(m_gui.CreateWidget(groupBox, "TaharezLook/Button", glm::vec4(0.0f, XPOS + YDIM + 0.02f, 1.0f, 0.05f), glm::vec4(0.0f), "GenerateWorldButton"));
		btnGenWorld->setText("Generate World");
		btnGenWorld->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MainScreen::OnGenWolrdClicked, this));
	}

	{//Selection Modes
		float XPOS = 0.0f;
		float YPOS = btnGenWorld->getYPosition().d_scale + btnGenWorld->getHeight().d_scale + 0.02;
		float XDIM = 0.05f;
		float YDIM = 0.02f;
		float Offset = 0.25f;

		//DRAW MODE
		m_drawModeButton = static_cast<CEGUI::RadioButton*>(m_gui.CreateWidget(groupBox, "TaharezLook/RadioButton", glm::vec4(XPOS + Offset, YPOS, XDIM, YDIM), glm::vec4(0.0f), "DrawModeButton"));
		m_drawModeButton->setSelected(true);
		m_drawModeButton->setGroupID(GROUP_ID);
		m_drawModeButton->subscribeEvent(CEGUI::RadioButton::EventSelectStateChanged, CEGUI::Event::Subscriber(&MainScreen::OnDrawModeClicked, this));

		//ERASE MODE
		m_eraseModeButton = static_cast<CEGUI::RadioButton*>(m_gui.CreateWidget(groupBox, "TaharezLook/RadioButton", glm::vec4(XPOS + XDIM + Offset * 2, YPOS, XDIM, YDIM), glm::vec4(0.0f), "EraseModeButton"));
		m_eraseModeButton->setSelected(false);
		m_eraseModeButton->setGroupID(GROUP_ID);
		m_eraseModeButton->subscribeEvent(CEGUI::RadioButton::EventSelectStateChanged, CEGUI::Event::Subscriber(&MainScreen::OnEraseModeClicked, this));
	}
	
}

void MainScreen::UpdateMousePosition(const SDL_Event& evnt)
{

}

void MainScreen::UpdateMouseDown(const SDL_Event& evnt)
{

}

void MainScreen::UpdateMouseUp(const SDL_Event& evnt)
{

}

bool MainScreen::OnExitClicked(const CEGUI::EventArgs& e)
{
	m_currState = WolfEngineGL::ScreenState::EXIT;
	return true;
}

bool MainScreen::OnLvlWidthValueChange(const CEGUI::EventArgs& e)
{
	needsGen = true;
	return true;
}

bool MainScreen::OnLvlHeightValueChange(const CEGUI::EventArgs& e)
{
	needsGen = true;
	return true;
}

bool MainScreen::OnGenWolrdClicked(const CEGUI::EventArgs& e)
{
	lvlWidth = (int)lvlWidthSpinner->getCurrentValue();
	lvlHeight = (int)lvlHeightSpinner->getCurrentValue();

	needsGen = false;

	btnGenWorld->disable();

	std::printf("DEBUG: world has been generated.\n");

	m_levelData.clear();

	std::string temp;

	for (int y = 0; y < lvlHeight; y++)
	{
		for (int x = 0; x < lvlWidth; x++)
		{
			temp += EMPTY_TILE;

			if (x == lvlWidth - 1)
			{
				m_levelData.push_back(temp);
				temp.clear();
			}
		}
	}

	return true;
}

bool MainScreen::OnDrawModeClicked(const CEGUI::EventArgs& e)
{
	m_selectMode = SelectMode::DRAW;

	printf("DEBUG: SelectMode: DRAW\n");

	return true;
}

bool MainScreen::OnEraseModeClicked(const CEGUI::EventArgs& e)
{
	m_selectMode = SelectMode::ERASE;
	
	printf("DEBUG: SelectMode: ERASE\n");

	return true;
}