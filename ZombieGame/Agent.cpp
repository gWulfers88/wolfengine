#include "Agent.h"

#include <WolfEngineGL/ResourceManager.h>

#include "Level.h"
#include <algorithm>

Agent::Agent()
{
}

Agent::~Agent()
{
}

bool Agent::CollideWithLevel(const std::vector<std::string>& levelData)
{
	std::vector<glm::vec2> collideTilePos;

	//Check the four corners
	CheckTilePos(levelData,
		collideTilePos,
		m_Position.x,
		m_Position.y);

	//Check second corners
	CheckTilePos(levelData,
		collideTilePos,
		m_Position.x + AGENT_WIDTH,
		m_Position.y);

	//Check third corners
	CheckTilePos(levelData,
		collideTilePos,
		m_Position.x,
		m_Position.y + AGENT_WIDTH);

	//Check fourth corners
	CheckTilePos(levelData,
		collideTilePos,
		m_Position.x + AGENT_WIDTH,
		m_Position.y + AGENT_WIDTH);

	if (collideTilePos.size() == 0)
	{
		return false;
	}

	for (size_t i = 0; i < collideTilePos.size(); i++)
	{
		CollideWithTile(collideTilePos[i]);
	}

	return true;
}


bool Agent::CollideWithAgent(Agent* agent, Grid* grid)
{
	const float MinDist = agentRad * 2;

	glm::vec2 centerPosA = m_Position + glm::vec2(agentRad);
	glm::vec2 centerPosB = agent->GetPosition() + glm::vec2(agentRad);

	glm::vec2 distVec = centerPosA - centerPosB;

	float dist = glm::length(distVec);

	float colDepth = MinDist - dist;

	if (colDepth > 0)
	{
		glm::vec2 colVec = glm::normalize(distVec) * colDepth;

		m_Position += colVec / 2.0f;
		agent->m_Position -= colVec / 2.0f;

		return true;
	}

	return false;
}


bool Agent::ApplyDamage(float damage)
{
	m_health -= damage;

	if (m_health <= 0)
	{
		return true;
	}

	return false;
}


void Agent::CheckTilePos(const std::vector<std::string>& levelData,
							std::vector<glm::vec2>& collideTilePos,
													float x, float y)
{
	glm::vec2 cornerPos = glm::vec2(floor(x / (float)TILE_WIDTH),
					floor(y / (float)TILE_WIDTH));

	//if we are outside of the world
	if (cornerPos.x < 0 || cornerPos.x >= levelData[0].length() ||
		cornerPos.y < 0 || cornerPos.y >= levelData.size())
	{
		return;
	}

	if (levelData[(int)cornerPos.y][(int)cornerPos.x] != '.')
	{
		collideTilePos.push_back(cornerPos  * (float)TILE_WIDTH + glm::vec2(TILE_WIDTH / 2.0f));
	}
}

void Agent::CollideWithTile(glm::vec2 tilePos)
{
	//AABB Col
	const float TILE_RAD = (float)TILE_WIDTH / 2.0f;
	const float MinDist = agentRad + TILE_RAD;

	glm::vec2 centerPlayer = m_Position + glm::vec2(agentRad);
	glm::vec2 distVec = centerPlayer - tilePos;

	float xdepth = MinDist - abs(distVec.x);
	float ydepth = MinDist - abs(distVec.y);

	if (xdepth > 0 || ydepth > 0)
	{
		if (std::max(xdepth, 0.0f) < std::max(ydepth, 0.0f))
		{
			if (distVec.x < 0)
			{
				m_Position.x -= xdepth;
			}
			else
			{
				m_Position.x += xdepth;
			}
			
		}
		else
		{
			if (distVec.y < 0)
			{
				m_Position.y -= ydepth;
			}
			else
			{
				m_Position.y += ydepth;
			}
		}
	}
}

void Agent::Draw(WolfEngineGL::SpriteBatch& spriteBatch)
{
	glm::vec4 destRect;
	destRect.x = m_Position.x;
	destRect.y = m_Position.y;
	destRect.z = AGENT_WIDTH;
	destRect.w = AGENT_WIDTH;

	const glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);
	
	spriteBatch.Draw(destRect, uvRect, m_textureID, 0.0f, color, m_dir);
}