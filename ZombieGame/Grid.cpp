#include "Grid.h"


Grid::Grid(int width, int height, int cellSize)
	: m_width(width),
	m_height(height),
	m_cellSize(cellSize)
{
	m_numXCells = (int)ceil((float)m_width / m_cellSize);
	m_numYCells = (int)ceil((float)m_height / m_cellSize);

	//Allocate all the cells
	const int AGENTS_TO_RESERVE = 20;
	m_cells.resize(m_numYCells * m_numXCells);
	for (size_t i = 0; i < m_cells.size(); i++)
	{
		m_cells[i].humans.reserve(AGENTS_TO_RESERVE);
	}

}

Grid::~Grid()
{

}

void Grid::AddAgent(Human* human)
{
	Cell* cell = GetCell(human->GetPosition());
	cell->humans.push_back(human);
	human->m_ownerCell = cell;
	human->cellIndex = cell->humans.size() - 1;
}

void Grid::AddAgent(Human* human, Cell* cell)
{
	cell->humans.push_back(human);
	human->m_ownerCell = cell;
	human->cellIndex = cell->humans.size() - 1;
}

void Grid::RemoveAgent(Human* human)
{
	//Vector swap
	human->m_ownerCell->humans[human->cellIndex] = human->m_ownerCell->humans.back();
	human->m_ownerCell->humans.pop_back();
	//Update vector index
	if (human->cellIndex < (signed)human->m_ownerCell->humans.size())
	{
		human->m_ownerCell->humans[human->cellIndex]->cellIndex = human->cellIndex;
	}
	//Set the index of the ball to -1 
	human->cellIndex = -1;
	human->m_ownerCell = nullptr;
}

Cell* Grid::GetCell(int x, int y)
{
	if (x < 0)
		x = 0;
	if (x >= m_numXCells)
		x = m_numXCells - 1;

	if (y < 0)
		y = 0;
	if (y >= m_numYCells)
		y = m_numYCells - 1;

	return &m_cells[y * m_numXCells + x];
}

Cell* Grid::GetCell(const glm::vec2& pos)
{
	if (pos.x < 0)
		return &m_cells[0];
	
	if (pos.x >= m_width)
		return &m_cells[0];

	if (pos.y < 0)
		return &m_cells[0];

	if (pos.y >= m_height)
		return &m_cells[0];

	int cellX = int(pos.x / m_cellSize);
	int cellY = int(pos.y / m_cellSize);

	return GetCell(cellX, cellY);

}