#include "Gun.h"

#include <glm/gtx/rotate_vector.hpp>

#include <random>
#include <ctime>

Gun::Gun(std::string name, int fireRate, int bulletsPerShot,
	float spread, float bulletDamage, float bulletSpeed,
	WeaponTypes weaponType, WolfEngineGL::SoundEffect shotEffect)
	: m_Name(name),
	m_FireRate(fireRate),
	m_BulletsPerShot(bulletsPerShot),
	m_Spread(spread),
	m_BulletDamage(bulletDamage),
	m_BulletSpeed(bulletSpeed),
	m_frameCounter(0),
	m_weaponType(weaponType),
	m_Ammo(0),
	m_shotEffect(shotEffect)
{
	switch (m_weaponType)
	{
	case WeaponTypes::Pistol:
	{
		m_Ammo = 15;
	}break;

	case WeaponTypes::SubmachineGun:
	{
		m_Ammo = 31;
	}break;

	case WeaponTypes::Shotgun:
	{
		m_Ammo = 12;
	}break;
	}
}

Gun::~Gun()
{
}

void Gun::Update(bool isMouseDown, const glm::vec2& position, const glm::vec2& direction, std::vector<Bullet>& bullets, float deltaTime)
{
	m_frameCounter += 1.0f * deltaTime;

	if (m_frameCounter >= m_FireRate && isMouseDown && m_Ammo != 0)
	{
		FireWeapon(direction, position, bullets);
		m_frameCounter = 0;
		m_Ammo--;
	}
}

void Gun::Reload()
{
	switch (m_weaponType)
	{
	case WeaponTypes::Pistol:
	{
		m_Ammo = 15;
	}break;

	case WeaponTypes::SubmachineGun:
	{
		m_Ammo = 31;
	}break;

	case WeaponTypes::Shotgun:
	{
		m_Ammo = 12;
	}break;
	}
}

void Gun::FireWeapon(const glm::vec2& direction, const glm::vec2& position, std::vector<Bullet>& bullets)
{
	static std::mt19937 randomEngine((unsigned int)time(nullptr));
	std::uniform_real_distribution<float> randRot(-m_Spread/20, m_Spread/20);

	m_shotEffect.Play();

	for (int i = 0; i < m_BulletsPerShot; i++)
	{
		bullets.emplace_back(position,
							glm::rotate(-direction, randRot(randomEngine)),
								(int)m_BulletDamage,
								m_BulletSpeed);
	}
}
