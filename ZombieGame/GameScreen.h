#pragma once

#include <WolfEngineGL/IGameScreen.h>
#include <WolfEngineGL/Camera2D.h>
#include <WolfEngineGL/GLSLProgram.h>

class GameScreen : public WolfEngineGL::IGameScreen
{
public:
	GameScreen(WolfEngineGL::Window* window);
	~GameScreen();

	virtual void Build() override;
	virtual void Destroy() override;

	virtual void OnEntry() override;
	virtual void OnExit() override;

	virtual void OnUpdate() override;
	virtual void OnDraw() override;

	virtual int GetNextIndex() const override;
	virtual int GetPrevIndex() const override;

private:
	void CheckInput();

	WolfEngineGL::Window* m_window;
	WolfEngineGL::Camera2D m_mainCamera;
	WolfEngineGL::GLSLProgram m_shaderProgram;

};
