#pragma once

#include <glm/glm.hpp>
#include <WolfEngineGL/SpriteBatch.h>

struct Cell;
class Grid;

const float AGENT_WIDTH = 60.0f;
const float agentRad = AGENT_WIDTH / 2.0f;

class Zombie;
class Human;

class Agent
{
public:
	Agent();
	virtual ~Agent();

	virtual void Update(Grid* grid, const std::vector<std::string>& levelData,
						std::vector<Human*>& humans,
						std::vector<Zombie*>& zombies, float deltaTime) = 0;

	bool CollideWithLevel(const std::vector<std::string>& levelData);

	bool CollideWithAgent(Agent* agent, Grid* grid);

	void Draw(WolfEngineGL::SpriteBatch& spriteBatch);

	glm::vec2 GetPosition() const { return m_Position; }

	bool ApplyDamage(float damage);
protected:
	void CheckTilePos(const std::vector<std::string>& levelData, 
		std::vector<glm::vec2>& collideTilePos,
		float x, float y);

	void CollideWithTile(glm::vec2 tilePos);

	glm::vec2 m_Position;
	WolfEngineGL::Color color;

	float m_Speed;

	float m_health;
	GLuint m_textureID;
	glm::vec2 m_dir = glm::vec2(1.0f, 0.0f);

public:
	Cell* m_ownerCell = nullptr;
	int cellIndex = -1;
};

