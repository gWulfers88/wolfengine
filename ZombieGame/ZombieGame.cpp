#include "ZombieGame.h"

#include <WolfEngineGL/WolfEngineGL.h>
#include <WolfEngineGL/Timing.h>
#include <WolfEngineGL/Errors.h>
#include <WolfEngineGL/ResourceManager.h>

#include <glm/gtx/rotate_vector.hpp>

#include "Gun.h"
#include "Zombie.h"

#include <SDL/SDL.h>
#include <iostream>
#include <random>
#include <ctime>

const float HUMAN_SPEED = 1.0f;
const float ZOMBIE_SPEED = 1.3f;

ZombieGame::ZombieGame()
	: m_ScrnWidth(1024),
	m_ScrnHeight(768),
	m_GameState(GameState::PLAY),
	fps(0),
	m_CurrLvl(0),
	m_Player(nullptr),
	numHumansKilled(0),
	numZombiesKilled(0),
	m_SpriteFont(nullptr),
	m_bloodParticleBatch(nullptr)
{

}

ZombieGame::~ZombieGame()
{
	for (size_t i = 0; i < m_Levels.size(); i++)
	{
		delete m_Levels[i];
	}

	for (size_t i = 0; i < m_Humans.size(); i++)
	{
		delete m_Humans[i];
	}

	for (size_t i = 0; i < m_Zombies.size(); i++)
	{
		delete m_Zombies[i];
	}

	if (m_SpriteFont)
	{
		delete m_SpriteFont;
	}
	
	m_AudioEngine.Destroy();

	SDL_Quit();
}

void ZombieGame::Run()
{
	InitSystems();

	GameLoop();
}

void updateBlood(WolfEngineGL::Particle2D& particle, float deltaTime)
{
	particle.m_position += particle.m_velocity * deltaTime;
	particle.m_color.A = GLubyte(particle.m_life * 255);
}

void ZombieGame::InitSystems()
{
	WolfEngineGL::Init();

	m_AudioEngine.Init();

	m_Window.CreateWindow("WolfEngineGL - Zombie Game", m_ScrnWidth, m_ScrnHeight, 0);

	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

	InitShaders();

	m_SpriteBatch.Init();
	m_HudSpriteBatch.Init();

	m_Camera.Init(m_ScrnWidth, m_ScrnHeight);

	m_HudCamera.Init(m_ScrnWidth, m_ScrnHeight);
	m_HudCamera.SetPosition(glm::vec2(m_ScrnWidth / 2, m_ScrnHeight / 2));

	//Init particles
	m_bloodParticleBatch = new WolfEngineGL::ParticleBatch2D();
	m_bloodParticleBatch->Init(1000, 0.05f, WolfEngineGL::ResourceManager::GetTexture("Textures/particle.png"), [](WolfEngineGL::Particle2D& particle, float deltaTime) 
	{
		particle.m_position += particle.m_velocity * deltaTime;
		particle.m_color.A = GLubyte(particle.m_life * 255);
	});

	m_ParticleSystem.AddParticleBatch(m_bloodParticleBatch);

	LoadContent();
}

void ZombieGame::InitShaders()
{
	//Compile our color shader
	m_ShaderProgram.CompileShaders("Shaders/textureShading.vert", "Shaders/textureShading.frag");
	
	// Adding Attributes for use with shaders
	m_ShaderProgram.AddAttributes("vertexPosition");
	m_ShaderProgram.AddAttributes("vertexColor");
	m_ShaderProgram.AddAttributes("vertexUV");

	//Link shader to program
	m_ShaderProgram.LinkShader();
}

void ZombieGame::LoadContent()
{
	//Load Audio files
	WolfEngineGL::Music music = m_AudioEngine.LoadMusic("Sound/XYZ.ogg");
	music.Play();

	//Load Fonts
	m_SpriteFont = new WolfEngineGL::SpriteFont("Fonts/28_Days_Later.ttf", 32);

	//Level One
	m_Levels.push_back(new Level("Levels/level2.txt"));
	m_CurrLvl = 0;

	//GRID
	m_grid = std::make_unique<Grid>(m_ScrnWidth, m_ScrnHeight, CELL_SIZE);

	//Player
	m_Player = new Player();
	m_Player->Init(5.0f, m_Levels[m_CurrLvl]->GetPlayerStart(), &m_InputManager, &m_Camera, &m_Bullets);

	//Add Humans
	m_Humans.push_back(m_Player);

	std::mt19937 randomEngine;
	randomEngine.seed((unsigned long)time(nullptr));

	std::uniform_int_distribution<int> randX(1, m_Levels[m_CurrLvl]->GetWidth() - 2);
	std::uniform_int_distribution<int> randY(1, m_Levels[m_CurrLvl]->GetHeight() - 2);

	for (int i = 0; i < m_Levels[m_CurrLvl]->GetNumHumans(); i++)
	{
		m_Humans.push_back(new Human);
		glm::vec2 pos(randX(randomEngine) * TILE_WIDTH, randY(randomEngine) * TILE_WIDTH);
		m_Humans.back()->Init(HUMAN_SPEED, pos);

		//Add the ball to the grid.
		//If you ever call emplace back after INIT balls.
		//m_grid will have DANGLING POINTERS!
		m_grid->AddAgent(m_Humans.back());
	}

	//Add Zombies
	const std::vector<glm::vec2>& zombiePos = m_Levels[m_CurrLvl]->GetZombieStart();

	for (size_t i = 0; i < zombiePos.size(); i++)
	{
		m_Zombies.push_back(new Zombie);
		m_Zombies.back()->Init(ZOMBIE_SPEED, zombiePos[i]);
	}

	//Setup the players guns
	const float BULLET_SPEED = 20.0f;

	m_Player->AddGun(new Gun("Magnum", 15, 1, 2.0f, 30.0f, BULLET_SPEED, WeaponTypes::Pistol, m_AudioEngine.LoadSFX("Sound/shots/pistol.wav")));
	m_Player->AddGun(new Gun("Shotgun", 30, 6, 2.0f, 4.0f, BULLET_SPEED, WeaponTypes::Shotgun, m_AudioEngine.LoadSFX("Sound/shots/shotgun.wav")));
	m_Player->AddGun(new Gun("MP5", 2, 1, 5.0f, 2.0f, BULLET_SPEED, WeaponTypes::SubmachineGun, m_AudioEngine.LoadSFX("Sound/shots/cg1.wav")));
}

void ZombieGame::GameLoop()
{
	WolfEngineGL::FPSLimiter fpsLim;
	fpsLim.SetTargetFPS(60.0f);

	const int MAX_STEPS = 5;

	const float MS_PER_SECOND = 1000;
	const float DESIRED_FRAMETIME = MS_PER_SECOND / fpsLim.GetTargetFPS();
	const float MAX_DELTATIME = 1.0f;

	const float CAM_SCALE = 1.0f / 2.0f;
	m_Camera.SetScale(CAM_SCALE);

	float prevTicks = (float)SDL_GetTicks();

	while (m_GameState != GameState::EXIT)
	{
		fpsLim.Start();

		float newTicks = (float)SDL_GetTicks();
		float frameTime = newTicks - prevTicks;
		prevTicks = newTicks;
		float totalDeltaTime = frameTime / DESIRED_FRAMETIME;

		if (m_Zombies.empty())
		{
			std::printf("You killed %d civilians, %d zombies. There are %d/%d civilians alive.", numHumansKilled, numZombiesKilled, m_Humans.size(), m_Levels[m_CurrLvl]->GetNumHumans() - 1);

			WolfEngineGL::fatalError("");
		}

		m_InputManager.Update();

		ProcessInput();

		int i = 0;

		while (totalDeltaTime > 0.0f && i < MAX_STEPS)
		{
			float deltaTime = std::fmin(totalDeltaTime, MAX_DELTATIME);

			if (m_GameState == GameState::PLAY)
			{
				Update(deltaTime);
			}

			totalDeltaTime -= deltaTime;

			i++;
		}

		Draw();

		fps = fpsLim.Stop();

		std::cout << "FPS: " << fps << std::endl;
	}
}

void ZombieGame::ProcessInput()
{
	SDL_Event e;

	while (SDL_PollEvent(&e))
	{
		switch (e.type)
		{
		case SDL_QUIT:
			m_GameState = GameState::EXIT;
			break;

		case SDL_MOUSEMOTION:
		{
			m_InputManager.SetMouseCoords((float)e.motion.x, (float)e.motion.y);
		}break;

		case SDL_KEYDOWN:
			m_InputManager.KeyPressed(e.key.keysym.sym);
			break;

		case SDL_KEYUP:
			m_InputManager.KeyReleased(e.key.keysym.sym);
			break;

		case SDL_MOUSEBUTTONDOWN:
			m_InputManager.KeyPressed(e.button.button);
			break;

		case SDL_MOUSEBUTTONUP:
			m_InputManager.KeyReleased(e.button.button);
			break;
		}
	}

	//Quit game if ESCAPE key is pressed.
	if (m_InputManager.isKeyPressed(SDLK_ESCAPE))
	{
		m_GameState = GameState::EXIT;
	}
}

float CamSpeed = 5.0f;

//Updates the game
void ZombieGame::Update(float deltaTime)
{
	m_ParticleSystem.Update(deltaTime);

	m_Camera.SetPosition(m_Player->GetPosition());
	m_Camera.Update();

	m_HudCamera.Update();
	
	for (size_t i = 0; i < m_Humans.size(); i++)
	{
		m_Humans[i]->Update(m_grid.get(), m_Levels[m_CurrLvl]->GetLevelData(),
							m_Humans, m_Zombies, deltaTime);
	}

	//Update Zombies
	for (size_t i = 0; i < m_Zombies.size(); i++)
	{
		m_Zombies[i]->Update(m_grid.get(), m_Levels[m_CurrLvl]->GetLevelData(),
			m_Humans, m_Zombies, deltaTime);
	}

	//Zombie Collisions
	for (size_t i = 0; i < m_Zombies.size(); i++)
	{
		for (size_t j = i + 1; j < m_Zombies.size(); j++)
		{
			m_Zombies[i]->CollideWithAgent(m_Zombies[j], m_grid.get());
		}

		//Zombie Human Collisions
		for (size_t j = 1; j < m_Humans.size(); j++)
		{
			if (m_Zombies[i]->CollideWithAgent(m_Humans[j], m_grid.get()))
			{
				m_Zombies.push_back(new Zombie);
				m_Zombies.back()->Init(ZOMBIE_SPEED, m_Humans[j]->GetPosition());
				
				delete m_Humans[j];
				m_Humans[j] = m_Humans.back();
				m_Humans.pop_back();
			}
		}

		//Collide with Player
		if (m_Zombies[i]->CollideWithAgent(m_Player, m_grid.get()))
		{
			addParticles(m_Player->GetPosition() + agentRad, 5);

			//WolfEngineGL::fatalError("You became a zombie. YOU LOSE!");
			m_GameState = GameState::GAMEOVER;
		}
	}
	
	//Collide with all other humans
	for (size_t i = 0; i < m_Humans.size(); i++)
	{
		for (size_t j = i + 1; j < m_Humans.size(); j++)
		{
			m_Humans[i]->CollideWithAgent(m_Humans[j], m_grid.get());
		}
	}

	//Update Bullets
	for (size_t i = 0; i < m_Bullets.size();)
	{
		if (m_Bullets[i].Update(m_Levels[m_CurrLvl]->GetLevelData(), deltaTime))
		{
			m_Bullets[i] = m_Bullets.back();
			m_Bullets.pop_back();
		}
		else
		{
			i++;
		}


		bool wasBulletRemoved;

		for (size_t i = 0; i < m_Bullets.size(); i++)
		{
			wasBulletRemoved = false;

			//loop through zombies
			for (size_t j = 0; j < m_Zombies.size();)
			{
				if (m_Bullets[i].CollideWithAgent(m_Zombies[j]))
				{
					addParticles(m_Bullets[i].GetPosition(), 5);

					//damage zombie and kill it
					if (m_Zombies[j]->ApplyDamage((float)m_Bullets[i].GetDamage()))
					{
						//if zombie died remove him
						delete m_Zombies[j];
						m_Zombies[j] = m_Zombies.back();
						m_Zombies.pop_back();

						numZombiesKilled++;
					}
					else
					{
						j++;
					}

					//remove bullet
					m_Bullets[i] = m_Bullets.back();
					m_Bullets.pop_back();
					wasBulletRemoved = true;
					i--;
					break;
				}
				else{
					j++;
				}
			}

			//loop through Humans
			if (wasBulletRemoved == false)
			{
				for (size_t j = 1; j < m_Humans.size();)
				{
					if (m_Bullets[i].CollideWithAgent(m_Humans[j]))
					{
						addParticles(m_Bullets[i].GetPosition(), 5);

						//damage zombie and kill it
						if (m_Humans[j]->ApplyDamage((float)m_Bullets[i].GetDamage()))
						{
							//if Human died remove him
							delete m_Humans[j];
							m_Humans[j] = m_Humans.back();
							m_Humans.pop_back();
							numHumansKilled++;
						}
						else
						{
							j++;
						}

						//remove bullet
						m_Bullets[i] = m_Bullets.back();
						m_Bullets.pop_back();
						i--;
						break;
					}
					else{
						j++;
					}
				}
			}
		}
	}

	std::printf("Zombies Killed: %d\nHumans Killed: %d\n", numZombiesKilled, numHumansKilled);
}

//Renders the game
void ZombieGame::Draw()
{
	//Set the base depth to 1.0
	glClearDepth(1.0f);
	//Clear the color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Binds the Shaders to the program
	m_ShaderProgram.Use();

	//Specify which Texture Component to use for render
	glActiveTexture(GL_TEXTURE0);

	//Gets the uniform var from our shader
	GLint textureUniform = m_ShaderProgram.GetUniformLoc("mySampler");
	glUniform1i(textureUniform, 0);	//binds it

	//gets the camera view matrix
	glm::mat4 camMat = m_Camera.GetCameraMatrix();
	GLint pUniform = m_ShaderProgram.GetUniformLoc("P");
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, &(camMat[0][0]));

	//Draw Level
	m_Levels[m_CurrLvl]->Draw();

	//Begin drawing agents
	m_SpriteBatch.Begin();

	const glm::vec2 agentSize(agentRad * 2.0f);

	for (size_t i = 0; i < m_Humans.size(); i++)
	{
		if (m_Camera.isInView(m_Humans[i]->GetPosition(), agentSize))
		{
			m_Humans[i]->Draw(m_SpriteBatch);
		}
	}

	for (size_t i = 0; i < m_Zombies.size(); i++)
	{
		if (m_Camera.isInView(m_Zombies[i]->GetPosition(), agentSize))
		{
			m_Zombies[i]->Draw(m_SpriteBatch);
		}
	}

	//Draw Bullets
	for (size_t i = 0; i < m_Bullets.size(); i++)
	{
		if (m_Camera.isInView(m_Bullets[i].GetPosition(), agentSize))
		{
			m_Bullets[i].Draw(m_SpriteBatch);
		}
	}

	m_SpriteBatch.End();

	m_SpriteBatch.Render();

	//Particles
	m_ParticleSystem.Draw(&m_SpriteBatch);

	DrawGUI();

	//unbinds the shaders
	m_ShaderProgram.Unuse();

	//Swap our buffer and draw everything to the screen
	m_Window.SwapBuffers();
}

void ZombieGame::DrawGUI()
{
	char buffer[256];

	//gets the camera view matrix
	glm::mat4 hudCamMat = m_HudCamera.GetCameraMatrix();
	GLint pUniform = m_ShaderProgram.GetUniformLoc("P");
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, &(hudCamMat[0][0]));

	m_HudSpriteBatch.Begin();

	//Human Count
	sprintf_s(buffer, "Humans: %d", m_Humans.size() - 1);
	m_SpriteFont->draw(m_HudSpriteBatch, buffer, glm::vec2(0, 0),
		glm::vec2( 1.0f ), 0.0f, WolfEngineGL::Color(255, 0, 0, 255));

	//Zombie Count
	sprintf_s(buffer, "Zombies: %d", m_Zombies.size() - 1);
	m_SpriteFont->draw(m_HudSpriteBatch, buffer, glm::vec2(0, 36),
		glm::vec2(1.0f), 0.0f, WolfEngineGL::Color(255, 0, 0, 255));

	//Ammo count
	sprintf_s(buffer, "Ammo: %d", m_Player->GetGun()->GetAmmo());
	m_SpriteFont->draw(m_HudSpriteBatch, buffer, glm::vec2(m_ScrnWidth - 200, 0),
		glm::vec2(1.0f), 0.0f, WolfEngineGL::Color(255, 0, 0, 255), WolfEngineGL::Justification::LEFT);

	//Gun type
	sprintf_s(buffer, "Weapon: %s", m_Player->GetGun()->GetName().c_str());
	m_SpriteFont->draw(m_HudSpriteBatch, buffer, glm::vec2(m_ScrnWidth - 300, 36),
		glm::vec2(1.0f), 0.0f, WolfEngineGL::Color(255, 0, 0, 255), WolfEngineGL::Justification::LEFT);

	//Reload
	if (m_Player->GetGun()->GetAmmo() == 0)
	{
		sprintf_s(buffer, "Press R to reload");
		m_SpriteFont->draw(m_HudSpriteBatch, buffer, glm::vec2(m_ScrnWidth / 2, 36),
			glm::vec2(1.0f), 0.0f, WolfEngineGL::Color(255, 0, 0, 255), WolfEngineGL::Justification::MIDDLE);
	}

	//Game Over
	if (m_GameState == GameState::GAMEOVER)
	{
		//Zombie Count
		sprintf_s(buffer, "GAME OVER. YOU DIED!");
		m_SpriteFont->draw(m_HudSpriteBatch, buffer, glm::vec2(m_ScrnWidth/2, m_ScrnHeight/2),
			glm::vec2(2.0f), 0.0f, WolfEngineGL::Color(255, 0, 0, 255), WolfEngineGL::Justification::MIDDLE);
	}

	m_HudSpriteBatch.End();
	m_HudSpriteBatch.Render();
}

void ZombieGame::addParticles(const glm::vec2& pos, int numParticles)
{
	static std::mt19937 randEngine((unsigned int)time(nullptr));
	static std::uniform_real_distribution<float> randAngle(0.0f, 360);

	glm::vec2 vel(2.0f, 0.0f);
	
	for (int i = 0; i < numParticles; i++)
	{
		m_bloodParticleBatch->AddParticle(pos, glm::rotate(vel, randAngle(randEngine)),
			WolfEngineGL::Color(255, 0, 0, 255), 20.0f);
	}
}