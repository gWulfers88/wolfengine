#pragma once

#include <string>
#include <vector>

#include <glm/glm.hpp>

#include <WolfEngineGL/AudioEngine.h>

#include "Bullet.h"

enum class WeaponTypes
{
	Pistol,
	SubmachineGun,
	Shotgun,
};

class Gun
{
public:
	Gun(std::string name, int fireRate, int bulletsPerShot,
		float spread, float bulletDamage, float bulletSpeed,
		WeaponTypes weaponType, WolfEngineGL::SoundEffect shotEffect);

	~Gun();

	void Update(bool isMouseDown, const glm::vec2& position, const glm::vec2& direction, std::vector<Bullet>& bullets, float deltaTime);

	std::string GetName() { return m_Name; }

	void Reload();
	int GetAmmo() const { return m_Ammo; }

private:
	void FireWeapon(const glm::vec2& direction, const glm::vec2& position, std::vector<Bullet>& bullets);

	std::string m_Name;
	int m_FireRate;
	int m_BulletsPerShot;
	float m_Spread;
	float m_BulletSpeed;
	float m_BulletDamage;

	float m_frameCounter;

	int m_Ammo;

	WeaponTypes m_weaponType;

	WolfEngineGL::SoundEffect m_shotEffect;
	
};

