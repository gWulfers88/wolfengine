#pragma once

#include "Human.h"
#include <WolfEngineGL/InputManager.h>
#include <WolfEngineGL/Camera2D.h>

#include "Bullet.h"

class Gun;

class Player : public Human
{
public:
	Player();
	~Player();

	void Init(float speed, glm::vec2 pos, WolfEngineGL::InputManager* inputManager, WolfEngineGL::Camera2D* camera,
		std::vector<Bullet>* bullets);
	void Update(Grid* grid, const std::vector<std::string>& levelData,
		std::vector<Human*>& humans,
		std::vector<Zombie*>& zombies, float deltaTime) override;

	void AddGun(Gun* gun);
	Gun* GetGun() { return m_Guns[m_currGunIndex]; }

private:
	WolfEngineGL::InputManager* m_InputManager;

	std::vector<Gun*> m_Guns;
	int m_currGunIndex;

	WolfEngineGL::Camera2D* m_Camera;

	std::vector<Bullet>* m_Bullets;
};

