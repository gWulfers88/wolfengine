#pragma once

#include <WolfEngineGL/Window.h>
#include <WolfEngineGL/GLSLProgram.h>
#include <WolfEngineGL/Camera2D.h>
#include <WolfEngineGL/InputManager.h>
#include <WolfEngineGL/SpriteBatch.h>
#include <WolfEngineGL/SpriteFont.h>
#include <WolfEngineGL/AudioEngine.h>
#include <WolfEngineGL/ParticleSystem2D.h>
#include <WolfEngineGL/ParticleBatch2D.h>

#include "Grid.h"
#include "Player.h"
#include "Level.h"
#include "Bullet.h"

#include <memory>

enum class GameState
{
	PLAY,
	GAMEOVER,
	EXIT,
};

class Zombie;

const int CELL_SIZE = 128;

class ZombieGame
{
public:
	ZombieGame();
	~ZombieGame();

	void Run();	//Runs the game

private:
	//Initializes the core systems
	void InitSystems();

	//Initializes the Shaders
	void InitShaders();

	//Load everything your game needs
	void LoadContent();

	//Main Game Loop
	void GameLoop();

	//Handles input processing
	void ProcessInput();

	//Updates the game
	void Update( float deltaTime );

	//Renders the game
	void Draw();

	void addParticles(const glm::vec2& pos, int numParticles);

	void DrawGUI();

	//Member variables
	WolfEngineGL::Window m_Window;

	WolfEngineGL::GLSLProgram m_ShaderProgram;

	WolfEngineGL::InputManager m_InputManager;

	//Main Camera
	WolfEngineGL::Camera2D m_Camera;
	//HUD-GUI Camera
	WolfEngineGL::Camera2D m_HudCamera;

	WolfEngineGL::SpriteBatch m_SpriteBatch;

	WolfEngineGL::SpriteFont* m_SpriteFont;
	WolfEngineGL::SpriteBatch m_HudSpriteBatch;

	WolfEngineGL::AudioEngine m_AudioEngine;

	WolfEngineGL::ParticleSystem2D m_ParticleSystem;

	WolfEngineGL::ParticleBatch2D* m_bloodParticleBatch;

	std::vector<Level*> m_Levels;

	std::unique_ptr<Grid> m_grid;	//Grid for spatial partitioning

	float fps;

	int m_ScrnWidth, m_ScrnHeight;

	int m_CurrLvl;

	Player* m_Player;

	std::vector<Human*> m_Humans;
	std::vector<Zombie*> m_Zombies;
	std::vector<Bullet> m_Bullets;

	int numHumansKilled;
	int numZombiesKilled;

	GameState m_GameState;
};

