#include "Game.h"

#include <WolfEngineGL/ScreenList.h>

#include "GameScreen.h"

Game::Game()
{
}

Game::~Game()
{
}

void Game::OnInit()
{

}

void Game::AddScreens()
{
	m_gameScreen = std::make_unique<GameScreen>(&m_window);

	m_screenList->AddScreen(m_gameScreen.get());
	m_screenList->SetScreen(m_gameScreen->GetIndex());
}

void Game::OnExit()
{
	
}