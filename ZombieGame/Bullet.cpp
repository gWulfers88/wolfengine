#include "Bullet.h"

#include <WolfEngineGL/ResourceManager.h>

#include "Level.h"
#include "Agent.h"
#include "Human.h"
#include "Zombie.h"

Bullet::Bullet(glm::vec2 pos, glm::vec2 direction, int damage, float speed)
	: m_Position(pos),
	m_Direction(direction),
	m_Damage(damage),
	m_Speed(speed)
{
}

Bullet::~Bullet()
{
}

bool Bullet::Update(std::vector<std::string>& levelData, float deltaTime)
{
	m_Position += m_Direction * m_Speed * deltaTime;

	return CollidWithWorld(levelData);
}

bool Bullet::CollideWithAgent(Agent* agent)
{
	const float MinDist = agentRad + BULLET_RADIUS;

	glm::vec2 centerPosA = m_Position;
	glm::vec2 centerPosB = agent->GetPosition() + glm::vec2(agentRad);

	glm::vec2 distVec = centerPosA - centerPosB;

	float dist = glm::length(distVec);

	float colDepth = MinDist - dist;

	if (colDepth > 0)
	{
		return true;
	}

	return false;
}

bool Bullet::CollidWithWorld(std::vector<std::string>& levelData)
{
	glm::ivec2 gridPos;
	gridPos.x = (int)floor(m_Position.x / TILE_WIDTH);
	gridPos.y = (int)floor(m_Position.y / TILE_WIDTH);

	//if we are outside of the world
	if (gridPos.x < 0 || gridPos.x >= (signed)levelData[0].length() ||
		gridPos.y < 0 || gridPos.y >= (signed)levelData.size())
	{
		if (levelData[gridPos.y][gridPos.x] == 'L')
		{
			levelData[gridPos.y][gridPos.x] = '.';
		}

		return true;
	}

	return ( levelData[gridPos.y][gridPos.x] != '.');
}

void Bullet::Draw(WolfEngineGL::SpriteBatch& spriteBatch)
{
	glm::vec4 destRect(m_Position.x + BULLET_RADIUS, m_Position.y + BULLET_RADIUS,
		BULLET_RADIUS * 2, BULLET_RADIUS * 2);

	const glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);

	WolfEngineGL::Color color = WolfEngineGL::Color(200, 200, 200, 255);

	spriteBatch.Draw(destRect, uvRect, WolfEngineGL::ResourceManager::GetTexture("Textures/circle.png").ID, 0, color);
}