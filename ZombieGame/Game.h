#pragma once

#include <WolfEngineGL/IMainGame.h>

class GameScreen;

class Game : public WolfEngineGL::IMainGame
{
public:
	Game();
	~Game();

	virtual void OnInit() override;
	virtual void AddScreens() override;
	virtual void OnExit() override;

private:
	std::unique_ptr<GameScreen> m_gameScreen = nullptr;
};

