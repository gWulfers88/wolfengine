#include "GameScreen.h"

#include <SDL/SDL.h>

GameScreen::GameScreen(WolfEngineGL::Window* window)
	: m_window(window)
{

}

GameScreen::~GameScreen()
{

}

void GameScreen::Build()
{
	
}

void GameScreen::Destroy()
{
	
}

void GameScreen::OnEntry()
{
	//Compile our vertex and fragment shaders
	m_shaderProgram.CompileShaders("Shaders/textureShading.vert", "Shaders/textureShading.frag");

	//Add some attributes that are in your shaders
	m_shaderProgram.AddAttributes("vertexPosition");
	m_shaderProgram.AddAttributes("vertexColor");
	m_shaderProgram.AddAttributes("vertexUV");

	//Link the shaders to the program
	m_shaderProgram.LinkShader();
}

void GameScreen::OnExit()
{

}

void GameScreen::OnUpdate()
{
	m_mainCamera.Update();

	CheckInput();
}

void GameScreen::OnDraw()
{
	//Clear buffers and color it
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

	//setup camera matrix
	glm::mat4 projMat = m_mainCamera.GetCameraMatrix();
	GLint pUniformLoc = m_shaderProgram.GetUniformLoc("P");
	glUniformMatrix4fv(pUniformLoc, 1, GL_FALSE, &projMat[0][0]);

	//Use the shaderProgram
	m_shaderProgram.Use();

	//Draw calls

	//Stop using the shaderProgram
	m_shaderProgram.Unuse();

}

int GameScreen::GetNextIndex() const
{
	return NO_SCREEN_INDEX;
}

int GameScreen::GetPrevIndex() const
{
	return NO_SCREEN_INDEX;
}

void GameScreen::CheckInput()
{
	SDL_Event evnt;

	while (SDL_PollEvent(&evnt))
	{
		m_game->OnSDLEvent(evnt);
	}
}