#include "Player.h"

#include <SDL/SDL.h>
#include <WolfEngineGL/ResourceManager.h>

#include "Gun.h"


Player::Player()
	: m_currGunIndex(-1)
{
}

Player::~Player()
{
	for (size_t i = 0; i < m_Guns.size(); i++)
	{
		if (m_Guns[i])
		{
			delete m_Guns[i];
		}
	}
}

void Player::Init(float speed, glm::vec2 pos, WolfEngineGL::InputManager* inputManager, WolfEngineGL::Camera2D* camera,
	std::vector<Bullet>* bullets)
{
	m_textureID = WolfEngineGL::ResourceManager::GetTexture("Textures/Top_Down_Survivor/rifle/idle/survivor-idle_rifle_0.png").ID;

	m_health = 100.0f;
	m_Speed = speed;
	m_Position = pos;

	color = WolfEngineGL::Color(255, 255, 255, 255);

	m_Camera = camera;
	m_Bullets = bullets;

	m_InputManager = inputManager;
}

void Player::AddGun(Gun* gun)
{
	m_Guns.push_back(gun);

	std::printf("You have equiped %s, Go kick some ASS!\n", gun->GetName().c_str());

	//If no current weapon is equiped, equip available
	if (m_currGunIndex == -1)
	{
		m_currGunIndex = 0;
	}
}

void Player::Update(Grid* grid, const std::vector<std::string>& levelData,
	std::vector<Human*>& humans,
	std::vector<Zombie*>& zombies, float deltaTime)
{
	if (m_InputManager->isKeyDown(SDLK_w))
	{
		m_Position.y += m_Speed * deltaTime;
	}

	if (m_InputManager->isKeyDown(SDLK_s))
	{
		m_Position.y -= m_Speed * deltaTime;
	}

	if (m_InputManager->isKeyDown(SDLK_a))
	{
		m_Position.x -= m_Speed * deltaTime;
	}

	if (m_InputManager->isKeyDown(SDLK_d))
	{
		m_Position.x += m_Speed * deltaTime;
	}
	
	if (m_InputManager->isKeyPressed(SDLK_1) && m_Guns.size() >= 0 )
	{
		m_currGunIndex = 0;
		std::printf("You have equiped %s, Go kick some ASS!\n", m_Guns[m_currGunIndex]->GetName().c_str());
	}
	else if (m_InputManager->isKeyPressed(SDLK_2) && m_Guns.size() >= 1)
	{
		m_currGunIndex = 1;
		std::printf("You have equiped %s, Go kick some ASS!\n", m_Guns[m_currGunIndex]->GetName().c_str());
	}
	else if (m_InputManager->isKeyPressed(SDLK_3) && m_Guns.size() >= 2)
	{
		m_currGunIndex = 2;
		std::printf("You have equiped %s, Go kick some ASS!\n", m_Guns[m_currGunIndex]->GetName().c_str());
	}

	if (m_InputManager->isKeyPressed(SDLK_r))
	{
		m_Guns[m_currGunIndex]->Reload();
	}

	glm::vec2 mouseCoords = m_InputManager->GetMouseCoords();
	mouseCoords = m_Camera->GetWorldCoordsFromScreen(mouseCoords);

	glm::vec2 centerPos = m_Position + glm::vec2(agentRad);

	m_dir = glm::normalize(centerPos - mouseCoords);

	if (m_currGunIndex != -1)
	{
		m_Guns[m_currGunIndex]->Update(m_InputManager->isKeyDown(SDL_BUTTON_LEFT),
			centerPos, m_dir, *m_Bullets, deltaTime);
	}

	CollideWithLevel(levelData);
}
