#include "Level.h"

#include <WolfEngineGL/Errors.h>
#include <WolfEngineGL/GLTexture.h>
#include <WolfEngineGL/ResourceManager.h>

#include <fstream>
#include <iostream>

Level::Level(const std::string& fileName)
	: m_NumHumans(0)
{
	std::ifstream file;
	file.open(fileName);

	if (file.fail())
	{
		WolfEngineGL::fatalError("Failed to open " + fileName);
	}

	std::string temp;
	
	file >> temp >> m_NumHumans;

	//throw away the rest of the first line
	std::getline(file, temp);

	//Read the level data
	while (std::getline(file, temp))
	{
		m_levelData.push_back(temp);
	}

	m_SpriteBatch.Init();
	m_SpriteBatch.Begin();

	//Draw here
	glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);

	WolfEngineGL::Color color = WolfEngineGL::Color(255, 255, 255, 255);

	//Render all tiles
	for (size_t y = 0; y < m_levelData.size(); y++)
	{
		for (size_t x = 0; x < m_levelData[y].size(); x++)
		{
			//Get the tile from the data
			char tile = m_levelData[y][x];

			//Get dest rect
			glm::vec4 destRect(x * TILE_WIDTH, y * TILE_WIDTH, TILE_WIDTH, TILE_WIDTH);

			//process tile
			switch (tile)
			{
			case 'B':
			case 'R':
			{
				m_SpriteBatch.Draw(destRect, uvRect,
				WolfEngineGL::ResourceManager::GetTexture("Textures/Walls/Wall_4.png").ID,
				0.0f, color);
			}break;

			case 'G':
				m_SpriteBatch.Draw(destRect, uvRect,
					WolfEngineGL::ResourceManager::GetTexture("Textures/Walls/Wall_4.png").ID,
					0.0f, color);
				break;

			case 'L':
				m_SpriteBatch.Draw(destRect, uvRect,
					WolfEngineGL::ResourceManager::GetTexture("Textures/Walls/Wall_5.png").ID,
					0.0f, color);
				break;

			case '@':
				m_SpriteBatch.Draw(destRect, uvRect,
					WolfEngineGL::ResourceManager::GetTexture("Textures/Floors/Grass_2.png").ID,
					0.0f, color);
				m_levelData[y][x] = '.';
				m_PlayerStart.x = float(x * TILE_WIDTH);
				m_PlayerStart.y = float(y * TILE_WIDTH);
				break;

			case 'Z':
				m_levelData[y][x] = '.';
				m_ZombieStart.emplace_back(x * TILE_WIDTH, y * TILE_WIDTH);
				break;

			case '.':
				m_SpriteBatch.Draw(destRect, uvRect,
					WolfEngineGL::ResourceManager::GetTexture("Textures/Floors/Grass_1.png").ID,
					0.0f, color);
				break;

			default:
				std::printf("Unexpected symbol %c at (%d, %d)!", tile, x, y);
				break;
			}
		}
	}

	m_SpriteBatch.End();
}

Level::~Level()
{
}

void Level::Draw()
{
	m_SpriteBatch.Render();
}