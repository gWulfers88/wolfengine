#pragma once

#include "Human.h"

#include <glm/glm.hpp>

#include <vector>

struct Cell
{
	std::vector<Human*> humans;
};

class Grid
{
public:
	Grid(int width, int height, int cellSize);
	~Grid();

	void AddAgent(Human* human);
	
	void AddAgent(Human* human, Cell* cell);

	//Get cells based on coords
	Cell* GetCell(int x, int y);

	//get cell based on position
	Cell* GetCell(const glm::vec2& pos);

	void RemoveAgent(Human* human);

private:
	int m_cellSize, m_width, m_height;
	int m_numXCells, m_numYCells;

	std::vector<Cell> m_cells;
};

