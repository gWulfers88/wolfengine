#include "Zombie.h"

#include <WolfEngineGL/ResourceManager.h>

#include "Human.h"

Zombie::Zombie()
{
}


Zombie::~Zombie()
{
}

void Zombie::Init(float speed, glm::vec2 pos)
{
	m_health = 95.0f;

	color = WolfEngineGL::Color(255, 255, 255, 255);

	m_Speed = speed;
	m_Position = pos;

	m_textureID = WolfEngineGL::ResourceManager::GetTexture("Textures/Top_Down_Survivor/Zombie/skeleton-attack_0.png").ID;
}

void Zombie::Update(Grid* grid, const std::vector<std::string>& levelData,
	std::vector<Human*>& humans,
	std::vector<Zombie*>& zombies, float deltaTime)
{

	Human* closestHuman = GetNearestHuman(humans);

	if (closestHuman != nullptr)
	{
		m_dir = glm::normalize(closestHuman->GetPosition() - m_Position);

		m_Position += m_dir * m_Speed * deltaTime;
	}

	CollideWithLevel(levelData);
}

Human* Zombie::GetNearestHuman(std::vector<Human*>& humans)
{
	Human* closestHuman = nullptr;

	float smallNum = 999999999.0f;

	for (size_t i = 0; i < humans.size(); i++)
	{
		glm::vec2 distVec = humans[i]->GetPosition() - m_Position;
		float dist = glm::length(distVec);

		if (dist < smallNum)
		{
			smallNum = dist;
			closestHuman = humans[i];
		}
	}

	return closestHuman;
}