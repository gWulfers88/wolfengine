#pragma once

#include <string>
#include <vector>

#include <WolfEngineGL/SpriteBatch.h>

const int TILE_WIDTH = 64;

class Level
{
public:
	Level(const std::string& fileName);
	~Level();

	void Draw();

	glm::vec2 GetPlayerStart() const { return m_PlayerStart; }
	const std::vector<glm::vec2>& GetZombieStart() const { return m_ZombieStart; }

	std::vector<std::string>& GetLevelData() { return m_levelData; }

	int GetNumHumans() { return m_NumHumans; }

	int GetWidth() const { return m_levelData[0].size(); }
	int GetHeight() const { return m_levelData.size(); }

private:
	std::vector<std::string> m_levelData;
	int m_NumHumans;

	WolfEngineGL::SpriteBatch m_SpriteBatch;
	glm::vec2 m_PlayerStart;
	std::vector<glm::vec2> m_ZombieStart;
};

