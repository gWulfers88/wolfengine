#pragma once

#include <string>
#include <glm/glm.hpp>

#include <WolfEngineGL/SpriteBatch.h>

class Agent;
class Human;
class Zombie;

const int BULLET_RADIUS = 5;

class Bullet
{
public:
	Bullet(glm::vec2 pos, glm::vec2 direction, int damage, float speed);
	~Bullet();

	bool Update(std::vector<std::string>& levelData, float deltaTime);

	void Draw(WolfEngineGL::SpriteBatch& spriteBatch);

	bool CollideWithAgent(Agent* agent);

	int GetDamage() { return m_Damage; }

	glm::vec2 GetPosition() { return m_Position; }

private:
	bool CollidWithWorld(std::vector<std::string>& levelData);

	glm::vec2 m_Position;
	glm::vec2 m_Direction;
	
	int m_Damage;
	float m_Speed;
};

