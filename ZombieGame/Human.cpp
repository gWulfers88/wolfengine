#include "Human.h"

#include <WolfEngineGL/ResourceManager.h>

#include <random>
#include <ctime>

#include <glm/gtx/rotate_vector.hpp>

Human::Human()
{
}


Human::~Human()
{
}

void Human::Init(float speed, glm::vec2 pos)
{
	m_textureID = WolfEngineGL::ResourceManager::GetTexture("Textures/Top_Down_Survivor/flashlight/idle/survivor-idle_flashlight_0.png").ID;

	m_health = 20.0f;

	static std::mt19937 randomEngine((unsigned int)time(nullptr));
	
	static std::uniform_real_distribution<float> randDir(-1.0f, 1.0f);

	color = WolfEngineGL::Color(255, 255, 255, 255);

	m_Speed = speed;
	m_Position = pos;

	m_dir = glm::vec2(randDir(randomEngine), randDir(randomEngine));

	if (m_dir.length() == 0)
	{
		m_dir = glm::vec2(1.0f, 0.0f);
	}

	m_dir = glm::normalize(m_dir);

}

void Human::Update(Grid* grid, const std::vector<std::string>& levelData,
	std::vector<Human*>& humans,
	std::vector<Zombie*>& zombies, float deltaTime)
{
	static std::mt19937 randomEngine((unsigned int)time(nullptr));
	static std::uniform_real_distribution<float> randRot(-45.0f, 45.0f);

	m_Position += m_dir * m_Speed * deltaTime;

	if (m_frames == 20)
	{
		m_dir = glm::rotate(m_dir, randRot(randomEngine));
		m_frames = 0;
	}
	else
	{
		m_frames++;
	}

	if (CollideWithLevel(levelData))
	{
		m_dir = glm::rotate(m_dir, randRot(randomEngine));
	}
}