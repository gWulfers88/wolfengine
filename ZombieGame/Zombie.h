#pragma once

#include "Agent.h"

class Zombie : public Agent
{
public:
	Zombie();
	~Zombie();

	void Init(float speed, glm::vec2 pos);

	void Update(Grid* grid, const std::vector<std::string>& levelData,
		std::vector<Human*>& humans,
		std::vector<Zombie*>& zombies, float deltaTime) override;

private:
	glm::vec2 direction;

	Human* GetNearestHuman(std::vector<Human*>& humans);
};

